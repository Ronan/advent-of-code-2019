package com.leroy.ronan.aoc2019;

public enum IntcodeOperation {
    ADD(4),
    MULTIPLY(4),
    READ(2),
    WRITE(2),
    JUMPIF(3),
    JUMPIFNOT(3),
    INF(4),
    EQL(4),
    OFFSET(2),
    HALT(0),
    ;

    private int size;

    IntcodeOperation(int size) {
        this.size = size;
    }

    public int getSize(){
        return size;
    }


}
