package com.leroy.ronan.aoc2019.day16;

import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class FlawedFrequencyTransmissionAlgorithm {

    private static final int[] BASE_PATTERN = {0,1,0,-1};

    private int[] stringToIntArray(String input) {
        return IntStream.range(0, input.length())
                .map(input::charAt)
                .map(c -> c - '0')
                .toArray();
    }

    private String intArrayToString(int[] input) {
        return Arrays.stream(input)
                .mapToObj(String::valueOf)
                .collect(Collectors.joining());
    }

    public String transform(String input) {
        return transform(input, 1, 0);
    }

    public String transform(String input, int phases, int offset) {
        return intArrayToString(transform(stringToIntArray(input), phases, offset));
    }

    private int[] transform(int[] input, int phases, int offset) {
        if (offset > input.length / 2) {
            input = bigTransform(input, phases, offset);
        } else {
            for (int i = 0; i < phases; i++) {
                input = transform(input, offset);
            }
        }
        return input;
    }

    private int[] bigTransform(int[] input, int phases, int offset) {
        int[] subArray = Arrays.copyOfRange(input, offset, input.length);
        for (int i = 0; i < phases; i++) {
            subArray = bigTransform(subArray);
        }
        var res = new int[input.length];
        System.arraycopy(input, 0, res, 0, offset);
        System.arraycopy(subArray, 0, res, offset, subArray.length);
        return res;
    }

    private int[] bigTransform(int[] input) {
        int[] res = new int[input.length];
        res[input.length-1] = input[input.length-1];
        for (int i = input.length -2; i >= 0; i--){
            res[i] = (input[i] + res[i+1])%10;
        }
        return res;
    }

    private int[] transform(int[] input, int offset) {
        var res = new int[input.length];
        System.arraycopy(input, 0, res, 0, offset);
        var transformed = IntStream.range(offset, input.length)
                .map(i -> applyPattern(input, i, offset))
                .map(i -> i % 10)
                .map(Math::abs)
                .toArray();
        System.arraycopy(transformed, 0, res, offset, transformed.length);
        return res;
    }

    private int applyPattern(int[] input, int index, int offset) {
        return IntStream.range(offset, input.length)
                .map(i -> input[i] * getPatternValue(i, index))
                .sum();
    }

    public int getPatternValue(int column, int line) {
        return BASE_PATTERN[(column+1)/(line+1) %4];
    }
}
