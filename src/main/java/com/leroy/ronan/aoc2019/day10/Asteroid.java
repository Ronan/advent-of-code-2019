package com.leroy.ronan.aoc2019.day10;

import java.util.Objects;

public class Asteroid {

    private final int x;
    private final int y;

    public Asteroid(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public boolean isBlocking(Asteroid source, Asteroid dest) {
        return ((dest.x - this.x) * (source.y - this.y) - (source.x - this.x) * (dest.y - this.y) == 0)
                && (dest.x - this.x) * (source.x - this.x) <= 0
                && (dest.y - this.y) * (source.y - this.y) <= 0;
    }

    public boolean isLocatedAt(int x, int y) {
        return this.x == x && this.y == y;
    }

    public double getAngleWith(Asteroid that) {
        double res;
        if (this.x <= that.x) {
            if (this.y <= that.y) {
                res = 2 * Math.PI - Math.atan((that.x - this.x) * 1.0 / (that.y - this.y));
            } else {
                res = Math.PI - Math.atan((that.x - this.x) * 1.0 / (that.y - this.y));
            }
        } else {
            if (this.y <= that.y) {
                res = -1.0 * Math.atan((that.x - this.x) * 1.0 / (that.y - this.y));
            } else {
                res = Math.PI - Math.atan((that.x - this.x) * 1.0 / (that.y - this.y));
            }
        }
        while (res >= 2 * Math.PI) {
            res = res - 2 * Math.PI;
        }
        return res;
    }

    @Override
    public String toString() {
        return "Asteroid{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Asteroid asteroid = (Asteroid) o;
        return x == asteroid.x &&
                y == asteroid.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }
}
