package com.leroy.ronan.aoc2019.day10;

import java.util.*;
import java.util.stream.Collectors;

public class AsteroidBelt {
    private int width;
    private int height;
    private List<Asteroid> asteroids;
    private Asteroid station;

    public AsteroidBelt(List<String> data) {
        this.width = data.get(0).length();
        this.height = data.size();
        this.asteroids = new ArrayList<>();
        for (int y = 0; y < data.size(); y++) {
            var line = data.get(y);
            for (int x = 0; x < line.length(); x++) {
                if ('#' == line.charAt(x)) {
                    asteroids.add(new Asteroid(x, y));
                } else if ('X' == line.charAt(x)) {
                    station = new Asteroid(x, y);
                    asteroids.add(station);
                }
            }
        }
    }

    public AsteroidBelt(int width, int height, List<Asteroid> asteroids, Asteroid station) {
        this.width = width;
        this.height = height;
        this.asteroids = asteroids;
        this.station = station;
    }

    @Override
    public String toString() {
        List<String> res = new ArrayList<>();
        for (int y = 0; y < height; y++) {
            StringBuilder line = new StringBuilder();
            for (int x = 0; x < width; x++) {
                if (station != null && station.isLocatedAt(x, y)) {
                    line.append('X');
                } else if (getAsteroidAt(x, y).isPresent()) {
                    line.append('#');
                } else {
                    line.append('.');
                }
            }
            res.add(line.toString());
        }
        return String.join("\n", res);
    }

    public Optional<Asteroid> getAsteroidAt(int x, int y) {
        return this.asteroids.stream().filter(a -> a.isLocatedAt(x, y)).findFirst();
    }

    public boolean areInView(Asteroid source, Asteroid dest) {
        for(Asteroid a : this.getAsteroids()) {
            if (!a.equals(source) && !a.equals(dest)) {
                if (a.isBlocking(source, dest)) {
                    return false;
                }
            }
        }
        return true;
    }

    private List<Asteroid> getAsteroids() {
        return asteroids;
    }

    public List<Asteroid> getAsteroidsInViewOf(Asteroid asteroid) {
        return this.getAsteroids().stream()
                .filter(a -> !a.equals(asteroid))
                .filter(a -> areInView(a, asteroid))
                .collect(Collectors.toList());
    }

    public Asteroid getBestLocation() {
        return this.getAsteroids().stream()
                .max(Comparator.comparing(a -> getAsteroidsInViewOf(a).size()))
                .orElse(null);
    }

    public Asteroid getMonitoringStation() {
        return station;
    }

    public void setMonitoringStation(int x, int y) {
        this.station = new Asteroid(x, y);
    }

    public AsteroidBelt vaporize(List<Asteroid> targets) {
        var remaining = asteroids.stream()
                .filter(a -> !targets.contains(a))
                .collect(Collectors.toList());
        return new AsteroidBelt(this.width, this.height, remaining, this.station);
    }

    public ArrayList<Asteroid> getAsteroidsInVaporizationOrder() {
        var monitoringStation = this.getMonitoringStation();
        var res = new ArrayList<Asteroid>();
        var belt = this;
        var currentWave = belt.getAsteroidsInViewOf(monitoringStation);
        while (currentWave.size() > 0) {
            res.addAll(currentWave.stream()
                    .sorted(Comparator.comparingDouble(a -> a.getAngleWith(monitoringStation)))
                    .collect(Collectors.toList()));
            belt = belt.vaporize(currentWave);
            currentWave = belt.getAsteroidsInViewOf(monitoringStation);
        }
        return res;
    }

}
