package com.leroy.ronan.aoc2019.day4;

public class SecureContainer {

    public boolean accepts(int input) {
        int[] array = String.valueOf(input)
                .chars()
                .toArray();
        int prevprev = 0;
        int prev = 0;
        boolean pair = false;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == prev && array[i] != prevprev && (i == 5 || array[i+1] != array[i])) {
                pair = true;
            } else if (array[i] < prev) {
                return false;
            }
            prevprev = prev;
            prev = array[i];
        }
        return pair;
    }
}
