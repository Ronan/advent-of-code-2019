package com.leroy.ronan.aoc2019.day15;

import com.leroy.ronan.aoc2019.Point;

import java.util.Random;

public enum Direction {
    NORTH(1) {
        @Override
        public Point getTargetFrom(Point point) {
            return new Point(point.getX(), point.getY() - 1);
        }
    },
    SOUTH(2) {
        @Override
        public Point getTargetFrom(Point point) {
            return new Point(point.getX(), point.getY() + 1);
        }
    },
    WEST(3) {
        @Override
        public Point getTargetFrom(Point point) {
            return new Point(point.getX() - 1, point.getY());
        }
    },
    EAST(4) {
        @Override
        public Point getTargetFrom(Point point) {
            return new Point(point.getX() + 1, point.getY());
        }
    },
    ;

    private long value;

    Direction(long value) {
        this.value = value;
    }

    public static Direction between(Point from, Point to) {
        Direction res = null;
        for(var direction : Direction.values()) {
            if (to.equals(direction.getTargetFrom(from))) {
                res = direction;
            }
        }
        return res;
    }

    public long getValue() {
        return value;
    }

    public abstract Point getTargetFrom(Point point);
}
