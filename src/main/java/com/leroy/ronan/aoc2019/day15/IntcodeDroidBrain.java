package com.leroy.ronan.aoc2019.day15;

import com.leroy.ronan.aoc2019.IntcodeComputer;

public class IntcodeDroidBrain implements DroidBrain {

    private IntcodeComputer computer;

    public IntcodeDroidBrain(IntcodeComputer computer) {
        this.computer = computer;
    }

    @Override
    public void push(long value) {
        computer.pushInput(value);
    }

    @Override
    public long pop() {
        return computer.popOutput()[0];
    }

    @Override
    public void run() {
        computer.run();
    }
}
