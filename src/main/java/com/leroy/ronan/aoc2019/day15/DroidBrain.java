package com.leroy.ronan.aoc2019.day15;

public interface DroidBrain {
    void push(long value);

    long pop();

    void run();
}
