package com.leroy.ronan.aoc2019.day15;

import java.util.stream.Stream;

public enum WorldStatus {
    WALL(0, "#", false),
    EMPTY(1, ".", false),
    TARGET(2, "T", true),
    FULL(3, "O", true),
    ;

    private long value;
    private String display;
    private boolean breathable;

    WorldStatus(long value, String display, boolean breathable) {
        this.value = value;
        this.display = display;
        this.breathable = breathable;
    }

    public boolean isBreathable() {
        return this.breathable;
    }

    @Override
    public String toString() {
        return this.display;
    }

    public static WorldStatus of(long value) {
        return Stream.of(WorldStatus.values())
                .filter(r -> r.value == value)
                .findAny()
                .orElse(null);
    }
}
