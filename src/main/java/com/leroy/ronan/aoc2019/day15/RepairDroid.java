package com.leroy.ronan.aoc2019.day15;

import com.leroy.ronan.aoc2019.Point;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class RepairDroid {

    private DroidBrain brain;
    private Point location;
    private AreaMap world;

    public RepairDroid(DroidBrain brain) {
        this.brain = brain;
        this.location = new Point(0, 0);
        this.world = new AreaMap();
        this.world.put(this.location, WorldStatus.EMPTY);
    }

    public AreaMap getAreaMap() {
        return world;
    }

    public Point getLocation() {
        return location;
    }

    public WorldStatus move(Direction direction) {
        var target = direction.getTargetFrom(this.location);
        brain.push(direction.getValue());
        brain.run();
        var response = WorldStatus.of(brain.pop());
        world.put(target, response);
        if (response != WorldStatus.WALL) {
            this.location = target;
        }
        return response;
    }

    public List<Point> explore() {
        List<Point> res = null;
        List<Point> path = new ArrayList<>();
        path.add(this.location);
        while (!world.isClosed()) {
            var direction = Stream.of(Direction.values())
                    .filter(dir -> !world.contains(dir.getTargetFrom(this.location)))
                    .findAny()
                    .orElseGet(() -> gotBack(path));
            var status = this.move(direction);
            while (path.contains(location)) {
                path.remove(path.size() -1);
            }
            path.add(location);
            if (status == WorldStatus.TARGET) {
                res = new ArrayList<>(path);
            }
        }
        return res;
    }

    private Direction gotBack(List<Point> path) {
        return Direction.between(path.get(path.size() - 1), path.get(path.size() - 2));
    }

    public int repair() {
        int time = 0;
        while (world.hasEmptyArea()) {
            world.fill(world.getAreaToFill());
            time++;
        }
        return time;
    }
}
