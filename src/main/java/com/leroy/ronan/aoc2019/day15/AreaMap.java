package com.leroy.ronan.aoc2019.day15;

import com.leroy.ronan.aoc2019.Point;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class AreaMap {

    private Map<Point, WorldStatus> world;

    public AreaMap() {
        this.world = new HashMap<>();
    }

    public void put(Point location, WorldStatus empty) {
        world.put(location, empty);
    }

    public boolean contains(Point point) {
        return world.containsKey(point);
    }

    public boolean isClosed() {
        return world.entrySet().stream()
                .filter(entry -> entry.getValue() != WorldStatus.WALL)
                .allMatch(entry -> this.isExplored(entry.getKey()));
    }

    private boolean isExplored(Point point) {
        return Stream.of(Direction.values())
                .allMatch(dir -> world.containsKey(dir.getTargetFrom(point)));
    }

    public String getMapAsString(boolean withOrigin, boolean withTarget, Point droidLocation) {
        var minX = world.keySet().stream().mapToInt(Point::getX).min().orElse(-1) -1;
        var maxX = world.keySet().stream().mapToInt(Point::getX).max().orElse(1) +1;
        var minY = world.keySet().stream().mapToInt(Point::getY).min().orElse(-1) -1;
        var maxY = world.keySet().stream().mapToInt(Point::getY).max().orElse(1) +1;

        return IntStream.range(minY, maxY+1)
                .mapToObj(y -> IntStream.range(minX, maxX+1)
                        .mapToObj(x -> getLocationAsString(x, y, withOrigin, withTarget, droidLocation))
                        .collect(Collectors.joining()))
                .collect(Collectors.joining("\n"));
    }

    private String getLocationAsString(int x, int y, boolean withOrigin, boolean withTarget, Point droidLocation) {
        var res = "";
        var location = new Point(x, y);
        var status = world.get(location);
        if (withOrigin && location.equals(new Point(0, 0))) {
            res = "O";
        } else if (location.equals(droidLocation)) {
            res = "D";
        } else if (withTarget && status == WorldStatus.TARGET) {
            res = "T";
        } else if (status == null) {
            res = " ";
        } else {
            res = status.toString();
        }
        return res;
    }

    public boolean hasEmptyArea() {
        return world.containsValue(WorldStatus.EMPTY);
    }

    public Set<Point> getAreaToFill() {
        return world.keySet().stream()
                .filter(p -> world.get(p).isBreathable())
                .map(this::getNeighboursToFill)
                .flatMap(Collection::stream)
                .collect(Collectors.toSet());
    }

    private Set<Point> getNeighboursToFill(Point point) {
        return Arrays.stream(Direction.values())
                .map(dir -> dir.getTargetFrom(point))
                .filter(p -> world.get(p) == WorldStatus.EMPTY)
                .collect(Collectors.toSet());
    }

    public void fill(Set<Point> areaToFill) {
        areaToFill.forEach(p -> world.put(p, WorldStatus.FULL));
    }
}