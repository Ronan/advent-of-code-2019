package com.leroy.ronan.aoc2019.day3;

import com.leroy.ronan.aoc2019.Point;

import java.util.*;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Circuit {

    private Point topRight;
    private Point bottomLeft;
    private List<List<Segment>> wires;
    private Set<Point> intersections = Collections.emptySet();

    public Circuit() {
        topRight = new Point(0, 0);
        bottomLeft = new Point(0, 0);
        this.wires = new ArrayList<>();
        this.wires.add(new ArrayList<>());
    }

    public Circuit(String input) {
        this();
        var source = new Point(0, 0);
        for (var dest : input.split(",")) {
            var x = source.getX();
            var y = source.getY();
            var length = Integer.parseInt(dest.substring(1));
            if (dest.startsWith("R")) {
                x = x + length;
                this.addSegment(Direction.RIGHT, source, new Point(source.getX() + length, source.getY()));
            } else if (dest.startsWith("U")) {
                y = y + length;
                this.addSegment(Direction.UP, source, new Point(source.getX(), source.getY() + length));
            } else if (dest.startsWith("L")) {
                x = x - length;
                this.addSegment(Direction.LEFT, source, new Point(source.getX() - length, source.getY()));
            } else if (dest.startsWith("D")) {
                y = y - length;
                this.addSegment(Direction.DOWN, source, new Point(source.getX(), source.getY() - length));
            }
            source = new Point(x, y);
        }
    }

    private void addSegment(Direction direction, Point source, Point target) {
        var segment = new Segment(direction, source, target);
        this.wires.get(0).add(segment);
        if (target.getX() > topRight.getX()) {
            topRight = new Point(target.getX(), topRight.getY());
        }
        if (target.getX() < bottomLeft.getX()) {
            bottomLeft = new Point(target.getX(), bottomLeft.getY());
        }
        if (target.getY() > topRight.getY()) {
            topRight = new Point(topRight.getX(), target.getY());
        }
        if (target.getY() < bottomLeft.getY()) {
            bottomLeft = new Point(bottomLeft.getX(), target.getY());
        }
    }

    public Circuit combinedWith(Circuit other) {
        var res = new Circuit();
        res.topRight = new Point(
                Math.max(this.topRight.getX(), other.topRight.getX()),
                Math.max(this.topRight.getY(), other.topRight.getY())
        );
        res.bottomLeft = new Point(
                Math.min(this.bottomLeft.getX(), other.bottomLeft.getX()),
                Math.min(this.bottomLeft.getY(), other.bottomLeft.getY())
        );
        res.wires = List.of(
                this.wires.get(0),
                other.wires.get(0)
        );
        res.intersections = res.getIntersections();
        return res;
    }

    private Set<Point> getIntersections() {
        var res = new HashSet<Point>();
        if (this.wires.size() == 2) {
            for (var current1 : this.wires.get(0)) {
                for (var current2 : this.wires.get(1)) {
                    var intersect = current1.intersect(current2);
                    intersect.ifPresent(res::add);
                }
            }
        }
        return res;
    }

    public int getClosestIntersectionDistance() {
        return intersections.stream()
                .filter(p -> p.getX() !=0 && p.getY() != 0)
                .min(Comparator.comparingInt(Point::getManhattanDistanceFromCenter))
                .map(Point::getManhattanDistanceFromCenter)
                .orElse(-1);
    }

    public int getStepsToClosestIntersection() {
        return this.intersections.stream()
                .filter(p -> p.getX() !=0 && p.getY() != 0)
                .min(Comparator.comparingInt(p -> this.getStepsTo(p)))
                .map(p -> this.getStepsTo(p))
                .orElse(-1);
    }

    private int getStepsTo(Point p) {
        return getStepsTo(p, this.wires.get(0))
            + getStepsTo(p, this.wires.get(1));
    }

    private int getStepsTo(Point p, List<Segment> segments) {
        int x = 0;
        int y = 0;
        int steps = 0;
        for (var segment : segments) {
            for (int i = 0; i < segment.getLength(); i++) {
                steps++;
                switch (segment.getDirection()) {
                    case RIGHT:
                        x++;
                        break;
                    case UP:
                        y++;
                        break;
                    case LEFT:
                        x--;
                        break;
                    case DOWN:
                        y--;
                        break;
                }
                if (p.getX() == x && p.getY() == y) {
                    return steps;
                }
            }
        }
        return steps;
    }

    public String toString() {
        return IntStream.range(bottomLeft.getY() - 1, topRight.getY() + 2)
                .boxed()
                .sorted(Collections.reverseOrder())
                .map(this::toStringLine)
                .collect(Collectors.joining("\n"));
    }

    private String toStringLine(int y) {
        return IntStream.range(bottomLeft.getX() - 1, topRight.getX() + 2)
                .mapToObj(x -> toStringPoint(x, y))
                .collect(Collectors.joining());
    }

    private String toStringPoint(int x, int y) {
        if(x == 0 && y == 0) {
            return "o";
        }
        if (this.intersections != null && this.intersections.contains(new Point(x, y))) {
            return "X";
        }
        String res = ".";
        for (var wire : this.wires) {
            for (var segment : wire) {
                if (segment.includes(x, y)) {
                    if (".".equals(res)) {
                        if (segment.getDirection().isHorizontal()) {
                            res = "-";
                        } else {
                            res = "|";
                        }
                    } else {
                        res = "+";
                    }
                }
            }
        }
        return res;
    }
}
