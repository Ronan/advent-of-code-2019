package com.leroy.ronan.aoc2019.day3;

public enum Direction {
    UP(false),
    LEFT(true),
    DOWN(false),
    RIGHT(true);

    private boolean horizontal;

    Direction(boolean horizontal) {
        this.horizontal = horizontal;
    }

    public boolean isHorizontal() {
        return horizontal;
    }
}
