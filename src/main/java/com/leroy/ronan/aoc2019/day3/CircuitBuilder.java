package com.leroy.ronan.aoc2019.day3;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class CircuitBuilder {

    private List<String> wires = new ArrayList<>();

    public CircuitBuilder fromFile(String name) throws URISyntaxException, IOException {
        var path = Paths.get(getClass().getClassLoader().getResource(name).toURI());
        Files.lines(path)
                .forEach(this::addWire);
        return this;
    }

    public CircuitBuilder addWire(String wire) {
        wires.add(wire);
        return this;
    }

    public Circuit build() {
        Circuit circuit = new Circuit();
        if (wires.size() == 1) {
            circuit = new Circuit(wires.get(0));
        } else if (wires.size() == 2) {
            var circuit1 = new Circuit(wires.get(0));
            var circuit2 = new Circuit(wires.get(1));
            return circuit1.combinedWith(circuit2);
        }
        return circuit;
    }
}
