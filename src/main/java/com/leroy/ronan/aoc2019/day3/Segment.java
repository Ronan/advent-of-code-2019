package com.leroy.ronan.aoc2019.day3;

import com.leroy.ronan.aoc2019.Point;

import java.util.Optional;

public class Segment {

    private final Direction direction;
    private final Point source;
    private final Point target;

    public Segment(Direction direction, Point source, Point target) {
        this.direction = direction;
        this.source = source;
        this.target = target;
    }

    public Direction getDirection() {
        return direction;
    }

    public int getLength() {
        return Math.abs(source.getX() - target.getX()) + Math.abs(source.getY() - target.getY());
    }

    public boolean includes(int x, int y) {
        if (direction.isHorizontal()) {
            return y == source.getY()
                    &&
                    (
                            source.getX() <= x && x <= target.getX()
                                    || target.getX() <= x && x <= source.getX()
                    );
        } else {
            return x == source.getX()
                    && (
                    source.getY() <= y && y <= target.getY()
                            || target.getY() <= y && y <= source.getY()
            );
        }
    }

    public Optional<Point> intersect(Segment other) {
        if (this.direction.isHorizontal() != other.direction.isHorizontal()) {
            int x;
            int y;
            if (this.direction.isHorizontal()) {
                x = other.source.getX();
                y = this.source.getY();
            } else {
                x = this.source.getX();
                y = other.source.getY();
            }
            if (this.includes(x, y) && other.includes(x, y)) {
                return Optional.of(new Point(x,y));
            }
        }
        return Optional.empty();
    }

}
