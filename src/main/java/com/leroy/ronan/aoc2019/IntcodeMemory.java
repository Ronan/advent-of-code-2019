package com.leroy.ronan.aoc2019;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class IntcodeMemory {

    private Map<Long, Long> content = new HashMap<>();

    public void setAll(long[] data) {
        content.clear();
        for (var i = 0L; i < data.length; i++) {
            content.put(i, data[(int)i]);
        }
    }

    public long[] dump() {
        var max = content.keySet().stream()
                .mapToLong(Long::longValue)
                .max()
                .orElse(0);
        var res = new long[(int)max+1];
        for (var i = 0; i < max+1; i++) {
            res[i] = get(i);
        }
        return res;
    }

    public void set(long address, long value) {
        log(address + " <- " + value);
        this.content.put(address, value);
    }

    public long get(long address) {
        return Optional.ofNullable(content.get(address)).orElse(0L);
    }
    private void log(String message) {
//        System.out.println(message);
    }
}
