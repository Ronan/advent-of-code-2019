package com.leroy.ronan.aoc2019;

import java.util.ArrayList;
import java.util.List;

public class IntcodeInputReader {

    private List<Long> data;

    public IntcodeInputReader() {
        this.data = new ArrayList<>();
    }

    public long read() {
        var res = data.get(0);
        data.remove(0);
        return res;
    }

    public boolean isEmpty() {
        return data.isEmpty();
    }

    public void pushInput(long[] input) {
        for (var i : input) {
            this.data.add(i);
        }
    }

    public void reset() {
        this.data = new ArrayList<>();
    }

    @Override
    public String toString() {
        return "IntcodeInputReader{" +
                "data=" + data +
                '}';
    }
}
