package com.leroy.ronan.aoc2019;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

public class IntcodeComputer {

    private IntcodeMemory memory = new IntcodeMemory();
    private IntcodeInputReader input = new IntcodeInputReader();
    private IntcodeOutputWriter output = new IntcodeOutputWriter();
    private boolean halt = false;
    private long instruction = 0;
    private long offset = 0;

    public void loadFromFile(String source) throws IOException, URISyntaxException {
        var path = Paths.get(getClass().getClassLoader().getResource(source).toURI());
        var line = Files.lines(path)
                .findFirst()
                .map(s -> s.split(","))
                .orElse(new String[0]);
        this.memory.setAll(Arrays.stream(line)
                .mapToLong(Long::valueOf)
                .toArray());
        this.instruction = 0;
        this.offset = 0;
    }

    public void load(long[] source) {
        this.memory.setAll(LongStream.of(source).toArray());
        this.instruction = 0;
        this.offset = 0;
    }

    public void setInput(long...input) {
        this.input.reset();
        this.input.pushInput(input);
    }

    public void pushInput(long...input) {
        this.input.pushInput(input);
    }

    public Long getOutput() {
        return output.last();
    }

    public long[] getOutputArray() {
        return output.all();
    }

    public long[] popOutput() {
        return output.pop();
    }

    public long[] getMemory() {
        return this.memory.dump();
    }

    public long getMemoryValueAt(long address) {
        return this.memory.get(address);
    }

    public long getCurrentInstruction() {
        return instruction;
    }

    public long getOffset() {
        return offset;
    }

    public void set(int address, int value) {
        this.memory.set(address, value);
    }

    private void setNoun(int noun) {
        set(1, noun);
    }

    private void setVerb(int verb) {
        set(2, verb);
    }

    private long getMemoryAtZero() {
        return memory.get(0);
    }

    public int[] getInputForGivenOutput(int output, String source) throws IOException, URISyntaxException {
        var noun = 0;
        while (noun < 99) {
            var verb = 0;
            while (verb < 99) {
                var res = this.runWithNounAndVerb(source, noun, verb);
                if (res == output) {
                    return new int[]{noun, verb};
                }
                verb++;
            }
            noun++;
        }
        return new int[0];
    }

    public long runWithNounAndVerb(String program, int noun, int verb) throws IOException, URISyntaxException {
        this.loadFromFile(program);
        this.setNoun(noun);
        this.setVerb(verb);
        this.run();
        return this.getMemoryAtZero();
    }

    public void run() {
        var opcode = new Opcode(memory.get(instruction));
        var stop = false;
        while(!stop) {
            OpcodeParameter[] params = getParameters(opcode);
            if ("01".equals(opcode.getOp())) {
                log(opcode + " ADD - " + memory.get(instruction + 1) + "-" + memory.get(instruction + 2) + "-" + memory.get(instruction + 3));
                memory.set(params[2].getAddress(), params[0].getValue() + params[1].getValue());
                instruction = instruction + IntcodeOperation.ADD.getSize();
            }else if ("02".equals(opcode.getOp())) {
                log(opcode + " MUL - " + memory.get(instruction + 1) + "-" + memory.get(instruction + 2) + "-" + memory.get(instruction + 3));
                memory.set(params[2].getAddress(), params[0].getValue() * params[1].getValue());
                instruction = instruction + IntcodeOperation.MULTIPLY.getSize();
            }else if ("03".equals(opcode.getOp())) {
                log(opcode + " READ - " + memory.get(instruction +1));
                if (input.isEmpty()) {
                    stop = true;
                } else {
                    memory.set(params[0].getAddress(), input.read());
                    instruction = instruction + IntcodeOperation.READ.getSize();
                }
            }else if ("04".equals(opcode.getOp())) {
                log(opcode + " WRITE - " + memory.get(instruction +1));
                output.write(params[0].getValue());
                instruction = instruction + IntcodeOperation.WRITE.getSize();
            }else if ("05".equals(opcode.getOp())) {
                log(opcode + " TRUE - " + memory.get(instruction +1) + "-" + memory.get(instruction +2));
                if (params[0].getValue() != 0) {
                    instruction = params[1].getValue();
                } else {
                    instruction = instruction + IntcodeOperation.JUMPIF.getSize();
                }
            }else if ("06".equals(opcode.getOp())) {
                log(opcode + " FALSE - " + memory.get(instruction +1) + "-" + memory.get(instruction +2));
                if (params[0].getValue() == 0) {
                    instruction = params[1].getValue();
                } else {
                    instruction = instruction + IntcodeOperation.JUMPIFNOT.getSize();
                }
            }else if ("07".equals(opcode.getOp())) {
                log(opcode + " INF - " + memory.get(instruction +1) + "-" + memory.get(instruction +2) + "-" + memory.get(instruction +3));
                if (params[0].getValue() < params[1].getValue()) {
                    memory.set(params[2].getAddress(), 1);
                } else {
                    memory.set(params[2].getAddress(), 0);
                }
                instruction = instruction + IntcodeOperation.INF.getSize();
            }else if ("08".equals(opcode.getOp())) {
                log(opcode + " EQL - " + memory.get(instruction +1) + "-" + memory.get(instruction +2) + "-" + memory.get(instruction +3));
                if (params[0].getValue() == params[1].getValue()) {
                    memory.set(params[2].getAddress(), 1);
                } else {
                    memory.set(params[2].getAddress(), 0);
                }
                instruction = instruction + IntcodeOperation.EQL.getSize();
            }else if ("09".equals(opcode.getOp())) {
                log(opcode + " OFF - " + memory.get(instruction +1));
                offset = offset + params[0].getValue();
                instruction = instruction + IntcodeOperation.OFFSET.getSize();
            }else if ("99".equals(opcode.getOp())) {
                log(opcode + " - HALT");
                this.halt = true;
                stop = true;
            } else {
                throw new IllegalArgumentException("Illegal instruction : " + opcode + " @ " + instruction);
            }
            opcode = new Opcode(memory.get(instruction));
        }
    }

    private OpcodeParameter[] getParameters(Opcode opcode) {
        return IntStream.of(1, 2, 3)
                .mapToObj(i -> opcode.getMode(i).getParameter(this, i))
                .toArray(OpcodeParameter[]::new);
    }

    public boolean isHalted() {
        return halt;
    }

    @Override
    public String toString() {
        return "IntcodeComputer{" +
                "input=" + input +
                ", output=" + output +
                ", halt=" + halt +
                ", instruction=" + instruction +
                '}';
    }

    private void log(String message) {
//        System.out.println(message);
    }
}
