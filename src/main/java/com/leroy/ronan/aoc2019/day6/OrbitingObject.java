package com.leroy.ronan.aoc2019.day6;

public class OrbitingObject {

    private final String name;
    private OrbitingObject parent;

    public OrbitingObject(String name, OrbitingObject parent) {

        this.name = name;
        this.parent = parent;
    }

    public String getName() {
        return name;
    }

    public OrbitingObject getParent() {
        return parent;
    }

    public void setParent(OrbitingObject parent) {
        this.parent = parent;
    }

    @Override
    public String toString() {
        return "OrbitingObject{" +
                "name='" + name + '\'' +
                ", parent=" + parent +
                '}';
    }
}
