package com.leroy.ronan.aoc2019.day6;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LocalOrbitsMap {

    private Map<String, OrbitingObject> all = new HashMap<>();

    public LocalOrbitsMap(String input) {
        Stream.of(input.split("\n"))
                .forEach(this::addLine);
    }

    private void addLine(String input) {
        var line = input.split("\\)");
        if (!exists(line[0])) {
            all.put(line[0], create(line[0], null));
        }
        if (!exists(line[1])) {
            all.put(line[1], create(line[1], getObjectByName(line[0])));
        } else {
            getObjectByName(line[1]).setParent(getObjectByName(line[0]));
        }
    }

    private OrbitingObject getObjectByName(String name) {
        return all.get(name);
    }

    private boolean exists(String name) {
        return all.containsKey(name);
    }

    private OrbitingObject create(String object, OrbitingObject parent) {
        return new OrbitingObject(object, parent);
    }

    public int getOrbitCount(String name) {
        var object = getObjectByName(name);
        if (object.getParent() == null) {
            return 0;
        } else {
            return getOrbitCount(object.getParent().getName()) + 1;
        }
    }

    public int getChecksum() {
        return all.keySet().stream()
                .mapToInt(this::getOrbitCount)
                .sum();
    }

    public List<String> getPathToCenter(String name) {
        if (getObjectByName(name).getParent() == null) {
            return List.of(name);
        } else {
            return Stream.concat(
                    Stream.of(name),
                    getPathToCenter(getObjectByName(name).getParent().getName()).stream()
            ).collect(Collectors.toList());
        }
    }

    public List<String> getCommonPathToCenter(String source, String target) {
        var sourcePath = getPathToCenter(source);
        var targetPath = getPathToCenter(target);
        return sourcePath.stream()
                .filter(targetPath::contains)
                .collect(Collectors.toList());
    }

    public int getMinimumOrbitalTransferNeeded(String source, String target) {
        var orbitalCountOfIntersection = getCommonPathToCenter(source, target).stream()
                .findFirst()
                .map(this::getOrbitCount)
                .orElse(0);
        return getOrbitCount(source) + getOrbitCount(target) - orbitalCountOfIntersection * 2 - 2;
    }
}
