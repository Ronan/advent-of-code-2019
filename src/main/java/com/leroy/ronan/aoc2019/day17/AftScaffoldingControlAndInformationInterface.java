package com.leroy.ronan.aoc2019.day17;

import com.leroy.ronan.aoc2019.IntcodeComputer;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class AftScaffoldingControlAndInformationInterface {

	private long[] image;

	public void load() throws IOException, URISyntaxException {
		var computer = new IntcodeComputer();
		computer.loadFromFile("day-17-input.txt");
		computer.run();
		image = computer.getOutputArray();
	}

	public String getImage() {
		return Arrays.stream(image)
				.mapToObj(i -> (char) i)
				.map(String::valueOf)
				.collect(Collectors.joining());
	}


	public int getWidth() {
		return Arrays.stream(getImage().split("\n")).mapToInt(String::length).max().orElse(0);
	}

	public int getHeight() {
		return getImage().split("\n").length;
	}

	public char charAt(int x, int y) {
		return getImage().split("\n")[y].charAt(x);
	}

	public boolean isIntersect(int x, int y) {
		return 0 < x && x < getWidth() - 1
				&& 0 < y && y < getHeight() - 1 && charAt(x, y) == '#'
				&& charAt(x - 1, y) == '#'
				&& charAt(x + 1, y) == '#'
				&& charAt(x, y - 1) == '#'
				&& charAt(x, y + 1) == '#';
	}

	public int getAlignmentParameter(int x, int y) {
		int res = 0;
		if (isIntersect(x, y)) {
			res = x * y;
		}
		return res;
	}

	public int getAlignmentParameter() {
		return IntStream.range(0, getWidth())
				.map(x -> IntStream.range(0, getHeight())
						.map(y -> getAlignmentParameter(x, y))
						.sum()
				).sum();
	}
}
