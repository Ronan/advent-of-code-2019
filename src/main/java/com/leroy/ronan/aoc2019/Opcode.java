package com.leroy.ronan.aoc2019;

public class Opcode {

    private final String opcode;

    public Opcode(long value) {
        this.opcode = opcodeToString(value);
    }

    private String opcodeToString(long opcode) {
        String opcodeStr = String.valueOf(opcode);
        while (opcodeStr.length() < 5) {
            opcodeStr = "0" + opcodeStr;
        }
        return opcodeStr;
    }

    public String getOp() {
        return opcode.substring(3);
    }

    public OpcodeParameterMode getMode(int param) {
        return OpcodeParameterMode.valueOf(opcode.charAt(3 - param));
    }

    @Override
    public String toString() {
        return opcode;
    }

}
