package com.leroy.ronan.aoc2019.day14;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class NanoFactory {

    private List<Reaction> reactions;

    public NanoFactory(List<String> input) {
        reactions = input.stream()
                .map(Reaction::new)
                .collect(Collectors.toList());
    }

    public NanoFactory(String source) throws IOException, URISyntaxException {
        var path = Paths.get(getClass().getClassLoader().getResource(source).toURI());
        reactions = Files.lines(path)
                .map(Reaction::new)
                .collect(Collectors.toList());
    }

    public long getOreRequirement(long qty, String element) {
        return getReactionProducing("ORE", qty, element).getNeedOf("ORE");
    }

    public Reaction getReactionProducing(String source, long qty, String result) {
        var reaction = getReactionProducing(qty, result);
        while (!Set.of(source).equals(reaction.getSourcesElements())) {
            for (var element : reaction.getSourcesElements()) {
                if (!source.equals(element)) {
                    reaction = reaction.add(getReactionProducing(reaction.getNeedOf(element), element));
                }
            }
            reaction = reaction.simplify();
        }
        return reaction;
    }

    public Reaction getReactionProducing(long qty, String result) {
        var reaction = reactions.stream()
                .filter(r -> r.getResultsElements().contains(result))
                .findAny()
                .orElse(null);
        if (reaction.getProductionOf(result) < qty) {
            var multiplier = (int) Math.ceil(1.0 * qty / reaction.getProductionOf(result));
            reaction = reaction.multiply(multiplier);
        }
        return reaction;
    }

    public long getMaxFuelFor(long oreAvailable) {
        long oreRequirementFor1Fuel = getOreRequirement(1, "FUEL");

        var fuelProducedPreviousTry = -1L;
        var oreConsumedPreviousTry = 0L;
        var fuelProducedCurrentTry = 0L;
        while (fuelProducedPreviousTry < fuelProducedCurrentTry) {
            var fuelProducedWithRemainingOre = (oreAvailable - oreConsumedPreviousTry) / oreRequirementFor1Fuel;
            fuelProducedPreviousTry = fuelProducedCurrentTry;
            fuelProducedCurrentTry = fuelProducedCurrentTry + fuelProducedWithRemainingOre;
            oreConsumedPreviousTry = this.getOreRequirement(fuelProducedCurrentTry, "FUEL");
        }
        var reaction = this.getReactionProducing("ORE", fuelProducedCurrentTry, "FUEL");
        while (reaction.getNeedOf("ORE") < oreAvailable) {
            fuelProducedCurrentTry++;
            reaction = this.getReactionProducing("ORE", fuelProducedCurrentTry, "FUEL");
        }
        return fuelProducedCurrentTry - 1;
    }
}
