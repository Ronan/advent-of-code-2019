package com.leroy.ronan.aoc2019.day14;

import java.util.Objects;

public class ElementAndQuantity {

    private long quantity;
    private String element;

    public ElementAndQuantity(String input) {
        var splitted = input.trim().split(" ");
        this.quantity = Long.parseLong(splitted[0].trim());
        this.element = splitted[1].trim();
    }

    public ElementAndQuantity(long quantity, String element) {
        this.quantity = quantity;
        this.element = element;
    }

    public long getQuantity() {
        return quantity;
    }

    public String getElement() {
        return element;
    }

    @Override
    public String toString() {
        return "ElementAndQuantity{" +
                "quantity=" + quantity +
                ", element='" + element + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ElementAndQuantity that = (ElementAndQuantity) o;
        return quantity == that.quantity &&
                Objects.equals(element, that.element);
    }

    @Override
    public int hashCode() {
        return Objects.hash(quantity, element);
    }
}
