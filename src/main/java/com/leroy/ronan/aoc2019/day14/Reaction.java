package com.leroy.ronan.aoc2019.day14;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Reaction {

    private Map<String, Long> sourcesMap;
    private Map<String, Long> resultsMap;

    public Reaction(String input) {
        String[] members = input.split("=>");
        String[] sources = members[0].split(",");
        this.sourcesMap = Arrays.stream(sources)
                .map(ElementAndQuantity::new)
                .collect(Collectors.toMap(
                        ElementAndQuantity::getElement,
                        ElementAndQuantity::getQuantity
                ));
        var result = new ElementAndQuantity(members[1].split(",")[0]);
        this.resultsMap = Stream.of(result)
                .collect(Collectors.toMap(
                        ElementAndQuantity::getElement,
                        ElementAndQuantity::getQuantity
                ));
    }

    public Reaction(Map<String, Long> sources, Map<String, Long> results) {
        this.sourcesMap = new HashMap<>();
        for (var element : sources.keySet()) {
            this.sourcesMap.put(element, sources.get(element));
        }
        this.resultsMap = new HashMap<>();
        for (var element : results.keySet()) {
            this.resultsMap.put(element, results.get(element));
        }
    }

    public Set<String> getSourcesElements() {
        return sourcesMap.entrySet().stream()
                .filter(entry -> entry.getValue() > 0L)
                .map(Map.Entry::getKey)
                .collect(Collectors.toSet());
    }

    public Set<String> getResultsElements() {
        return resultsMap.entrySet().stream()
                .filter(entry -> entry.getValue() > 0L)
                .map(Map.Entry::getKey)
                .collect(Collectors.toSet());
    }

    public Reaction simplify() {
        var sources = new HashMap<String, Long>();
        var results = new HashMap<String, Long>();
        for (var element : sourcesMap.keySet()) {
            sources.put(element, sourcesMap.get(element));
        }
        for (var element : resultsMap.keySet()) {
            if (sourcesMap.containsKey(element)) {
                var sourceQty = sourcesMap.get(element);
                var resultQty = resultsMap.get(element);
                if (resultQty < sourceQty) {
                    sources.put(element, sourceQty - resultQty);
                    results.remove(element);
                } else if (sourceQty < resultQty) {
                    sources.remove(element);
                    results.put(element, resultQty - sourceQty);
                } else {
                    sources.remove(element);
                    results.remove(element);
                }
            } else {
                results.put(element, resultsMap.get(element));
            }
        }
        return new Reaction(sources, results);
    }


    public Reaction add(Reaction other) {
        var sources = Stream.concat(
                this.getSourcesElements().stream(),
                other.getSourcesElements().stream()
        ).distinct()
                .map(element -> new ElementAndQuantity(
                        this.getNeedOf(element) + other.getNeedOf(element),
                        element
                ))
                .collect(Collectors.toMap(
                        ElementAndQuantity::getElement,
                        ElementAndQuantity::getQuantity
                ));
        var results = Stream.concat(
                this.getResultsElements().stream(),
                other.getResultsElements().stream()
        ).distinct()
                .map(element -> new ElementAndQuantity(
                        this.getProductionOf(element) + other.getProductionOf(element),
                        element
                ))
                .collect(Collectors.toMap(
                        ElementAndQuantity::getElement,
                        ElementAndQuantity::getQuantity
                ));
        return new Reaction(sources, results);
    }

    public Reaction multiply(long multiplier) {
        return new Reaction(
                sourcesMap.entrySet().stream()
                        .map(entry -> new ElementAndQuantity(entry.getValue(), entry.getKey()))
                        .map(eq -> new ElementAndQuantity(eq.getQuantity()*multiplier, eq.getElement()))
                        .collect(Collectors.toMap(
                                ElementAndQuantity::getElement,
                                ElementAndQuantity::getQuantity
                        )),
                resultsMap.entrySet().stream()
                        .map(entry -> new ElementAndQuantity(entry.getValue(), entry.getKey()))
                        .map(eq -> new ElementAndQuantity(eq.getQuantity()*multiplier, eq.getElement()))
                        .collect(Collectors.toMap(
                                ElementAndQuantity::getElement,
                                ElementAndQuantity::getQuantity
                        ))
        );
    }

    public Long getNeedOf(String element) {
        return sourcesMap.getOrDefault(element, 0L);
    }

    public long getProductionOf(String element) {
        return resultsMap.getOrDefault(element, 0L);
    }

    @Override
    public String toString() {
        return "Reaction{" +
                "sourcesMap=" + sourcesMap +
                ", resultsMap=" + resultsMap +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Reaction reaction = (Reaction) o;
        return Objects.equals(sourcesMap, reaction.sourcesMap) &&
                Objects.equals(resultsMap, reaction.resultsMap);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sourcesMap, resultsMap);
    }

}
