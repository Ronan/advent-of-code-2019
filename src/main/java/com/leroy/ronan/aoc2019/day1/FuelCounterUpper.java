package com.leroy.ronan.aoc2019.day1;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FuelCounterUpper {
    public int getRequirementForMass(int mass) {
        return mass / 3 -2;
    }

    public int getRequirementForModuleMass(int mass) {
        var res = 0;
        var requirement = getRequirementForMass(mass);
        while(requirement > 0) {
            res += requirement;
            requirement = getRequirementForMass(requirement);
        }
        return res;
    }

    public int getGlobalRequirement() throws URISyntaxException, IOException {
        var path = Paths.get(getClass().getClassLoader().getResource("day1-modules-mass.txt").toURI());
        var lines = Files.lines(path);
        return lines.mapToInt(Integer::valueOf)
                .map(this::getRequirementForModuleMass)
                .sum();
    }
}
