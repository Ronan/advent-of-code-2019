package com.leroy.ronan.aoc2019;

public enum OpcodeParameterMode {
    POSITION {
        public long getValue(IntcodeComputer computer, int paramIdx) {
            return computer.getMemoryValueAt(computer.getCurrentInstruction() + paramIdx);
        }
        public OpcodeParameter getParameter(IntcodeComputer computer, int paramIdx) {
            return new OpcodeParameter(
                    computer.getMemoryValueAt(computer.getCurrentInstruction() + paramIdx) ,
                    computer.getMemoryValueAt(computer.getMemoryValueAt(computer.getCurrentInstruction() + paramIdx))
            );
        }
    },
    DIRECT {
        public long getValue(IntcodeComputer computer, int paramIdx) {
            return 0L;
        }
        public OpcodeParameter getParameter(IntcodeComputer computer, int paramIdx) {
            return new OpcodeParameter(
                    computer.getMemoryValueAt(computer.getCurrentInstruction() + paramIdx),
                    computer.getMemoryValueAt(computer.getCurrentInstruction() + paramIdx)
            );
        }
    },
    RELATIVE {
        public long getValue(IntcodeComputer computer, int paramIdx) {
            return computer.getOffset() +
                   computer.getMemoryValueAt(computer.getCurrentInstruction() + paramIdx)
            ;
        }
        public OpcodeParameter getParameter(IntcodeComputer computer, int paramIdx) {
            return new OpcodeParameter(
                    computer.getOffset() + computer.getMemoryValueAt(computer.getCurrentInstruction() + paramIdx),
                    computer.getMemoryValueAt(computer.getOffset() + computer.getMemoryValueAt(computer.getCurrentInstruction() + paramIdx))
            );
        }
    };

    public static OpcodeParameterMode valueOf(char value) {
        switch (value) {
            case '0':
                return POSITION;
            case '1':
                return DIRECT;
            case '2':
                return RELATIVE;
            default:
                throw new IllegalArgumentException("Invalid OpcodeParameterMode : " + value);
        }
    }

    public abstract OpcodeParameter getParameter(IntcodeComputer computer, int param);
}
