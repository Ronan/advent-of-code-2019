package com.leroy.ronan.aoc2019.day8;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class SpaceImageBuilder {

    private String data;
    private int color;
    private int width;
    private int height;

    public SpaceImageBuilder data(String data) {
        this.data = data;
        return this;
    }

    public SpaceImageBuilder dataFromFile(String file) throws IOException, URISyntaxException {
        var path = Paths.get(getClass().getClassLoader().getResource(file).toURI());
        data = Files.lines(path)
                .findFirst()
                .orElse("");
        return this;
    }

    public SpaceImageBuilder fullOf(int color) {
        this.color = color;
        return this;
    }

    public SpaceImageBuilder size(int width, int height) {
        this.width = width;
        this.height = height;
        return this;
    }

    public SpaceImage build() {
        if (data != null) {
            List<SpaceImageLayer> builtLayers = new ArrayList<>();
            String[] layers = split(data, width * height);
            for (var layer : layers) {
                String[] lines = split(layer, width);
                builtLayers.add(new SpaceImageLayer(lines));
            }
            return new SpaceImage(height, width, builtLayers.toArray(new SpaceImageLayer[0]));
        } else {
            List<String> lines = new ArrayList<>();
            for (int y = 0; y < this.height; y++) {
                StringBuilder line = new StringBuilder();
                for (int x = 0; x < this.width; x++) {
                    line.append(color);
                }
                lines.add(line.toString());
            }
            return new SpaceImage(height, width, new SpaceImageLayer(lines.toArray(new String[0])));
        }
    }

    private String[] split(String data, int size) {
        List<String> res = new ArrayList<>();
        var index = 0;
        while(index + size <= data.length()) {
            res.add(data.substring(index, index+size));
            index += size;
        }
        return res.toArray(new String[0]);
    }
}
