package com.leroy.ronan.aoc2019.day8;

import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SpaceImageLayer {

    private String[] lines;

    public SpaceImageLayer(String...lines) {
        this.lines = lines;
    }

    public String[] getLines() {
        return lines;
    }

    public long count(int i) {
        return Stream.of(lines)
                .mapToLong(line -> line.chars().filter(c -> c == String.valueOf(i).charAt(0)).count())
                .sum();
    }

    public char get(int x, int y) {
        return this.getLines()[y].charAt(x);
    }

    public void set(int x, int y, int color) {
        var line = "";
        if (x > 0) {
            line += this.lines[y].substring(0, x);
        }
        line += (char)color;
        if (x < this.lines[y].length()) {
            line += this.lines[y].substring(x+1);
        }
        this.lines[y] = line;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SpaceImageLayer that = (SpaceImageLayer) o;
        return Arrays.equals(lines, that.lines);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(lines);
    }

    @Override
    public String toString() {
        return String.join("\n", lines);
    }
}
