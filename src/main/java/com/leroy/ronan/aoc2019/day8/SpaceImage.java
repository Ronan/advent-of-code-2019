package com.leroy.ronan.aoc2019.day8;

import java.util.Arrays;
import java.util.Comparator;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SpaceImage {
    private SpaceImageLayer[] layers;

    private int height;
    private int width;

    public SpaceImage(int height, int width, SpaceImageLayer... layers) {
        this.height = height;
        this.width = width;
        this.layers = layers;
    }

    public SpaceImageLayer[] getLayers() {
        return layers;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public SpaceImageLayer getLayerWithFewest(int i) {
        return Stream.of(layers)
                .min(Comparator.comparingLong(layer -> layer.count(i)))
                .orElse(null);
    }

    public long getChecksum() {
        var layer = this.getLayerWithFewest(0);
        return layer.count(1) * layer.count(2);
    }

    public SpaceImage render() {
        var result = new SpaceImageBuilder().fullOf(2).size(this.width, this.height).build().getLayers()[0];
        for(int x = 0; x < this.getWidth(); x++) {
            for(int y = 0; y < this.getHeight(); y++) {
                int color = -1;
                for (var layer : this.layers) {
                    var current = layer.get(x, y);
                    if (current != '2') {
                        color = current;
                        break;
                    }
                }
                result.set(x, y, color);
            }
        }
        return new SpaceImage(this.getHeight(), this.getWidth(), result);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SpaceImage that = (SpaceImage) o;
        return Arrays.equals(layers, that.layers);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(layers);
    }

    @Override
    public String toString() {
        return Stream.of(layers)
                .map(SpaceImageLayer::toString)
                .collect(Collectors.joining("\n--\n"));
    }
}
