package com.leroy.ronan.aoc2019;

import java.util.ArrayList;
import java.util.List;

public class IntcodeOutputWriter {

    private List<Long> value = new ArrayList<>();

    public Long last() {
        return value.get(value.size()-1);
    }

    public void write(long value) {
        this.value.add(value);
    }

    public long[] all() {
        return value.stream().mapToLong(Long::longValue).toArray();
    }

    @Override
    public String toString() {
        return "IntcodeOutputWriter{" +
                "value=" + value +
                '}';
    }

    public long[] pop() {
        var res = all();
        this.value.clear();
        return res;
    }
}
