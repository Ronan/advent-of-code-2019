package com.leroy.ronan.aoc2019.day7;

import com.leroy.ronan.aoc2019.IntcodeComputer;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.stream.LongStream;

public class ThrusterAmplifier {

    private long[] program;

    public ThrusterAmplifier(long[] program) {
        this.program = program;
    }

    public ThrusterAmplifier(String source) throws IOException, URISyntaxException {
        var path = Paths.get(getClass().getClassLoader().getResource(source).toURI());
        var line = Files.lines(path)
                .findFirst()
                .map(s -> s.split(","))
                .orElse(new String[0]);
        this.program = Arrays.stream(line)
                .mapToLong(Long::valueOf)
                .toArray();
    }

    public long amplify(long[] sequence) {
        var output = 0L;
        for (var phase : sequence) {
            IntcodeComputer computer = new IntcodeComputer();
            computer.load(program);
            computer.setInput(phase, output);
            computer.run();
            output = computer.getOutput();
        }
        return output;
    }

    public long[] search(long[] phases) {
        return search(phases, false);
    }

    public long[] search(long[] phases, boolean loop) {
        var maxAmplification = 0L;
        long[] maxSequence = {0,0,0,0,0};
        for (var a : phases) {
            for (var b : phases) {
                for (var c : phases) {
                    for (var d : phases) {
                        for (var e : phases) {
                            var sequence = new long[]{a,b,c,d,e};
                            if (isValid(sequence)) {
                                long amplification;
                                if (loop) {
                                    amplification = this.loopAmplify(sequence);
                                } else {
                                    amplification = this.amplify(sequence);
                                }
                                if (maxAmplification < amplification) {
                                    maxAmplification = amplification;
                                    maxSequence = sequence;
                                }
                            }
                        }
                    }
                }
            }
        }
        return maxSequence;
    }

    private boolean isValid(long[] sequence) {
        boolean res = true;
        for (var current : sequence) {
            if (LongStream.of(sequence).filter(x -> x == current).count() != 1) {
                res = false;
            }
        }
        return res;
    }

    public long loopAmplify(long[] sequence) {
        IntcodeComputer[] computers = new IntcodeComputer[]{
                new IntcodeComputer(),
                new IntcodeComputer(),
                new IntcodeComputer(),
                new IntcodeComputer(),
                new IntcodeComputer(),
        };
        for (int i = 0; i < computers.length; i++) {
            computers[i].load(program);
            computers[i].pushInput(sequence[i]);
        }
        long output = 0L;
        while (!computers[4].isHalted()) {
            for (var i = 0; i < computers.length; i++) {
                computers[i].pushInput(output);
                computers[i].run();
                output = computers[i].getOutput();
            }
        }
        return output;
    }
}
