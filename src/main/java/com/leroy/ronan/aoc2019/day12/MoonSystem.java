package com.leroy.ronan.aoc2019.day12;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class MoonSystem {

    private List<Moon> moons;

    public MoonSystem(List<String> data) {
        moons = data.stream()
                .map(Moon::new)
                .collect(Collectors.toList());
    }

    public MoonSystem(String filename) throws IOException, URISyntaxException {
        var path = Paths.get(getClass().getClassLoader().getResource(filename).toURI());
        moons = Files.lines(path)
                .map(Moon::new)
                .collect(Collectors.toList());

    }

    public List<Moon> getMoons() {
        return moons;
    }

    public void simulate(int steps) {
        for (int i = 0; i < steps; i ++) {
            this.simulate();
        }
    }

    public Vector stabilize(MoonSystem original) {
        var res = new Vector(0,0,0);
        var iteration = 0;
        while (res.getX() == 0 || res.getY() == 0 || res.getZ() == 0) {
            this.simulate();
            iteration++;
            if (this.isComponentEquals(original, Vector::getX)) {
                res.setX(iteration);
            }
            if (this.isComponentEquals(original, Vector::getY)) {
                res.setY(iteration);
            }
            if (this.isComponentEquals(original, Vector::getZ)) {
                res.setZ(iteration);
            }
        }
        return res;
    }

    public void simulate() {
        for (var moon : moons) {
            var acceleration = new Vector(0,0,0);
            for (var other : moons) {
                if (moon.getPosition().getX() < other.getPosition().getX()) {
                    acceleration.setX(acceleration.getX() + 1);
                } else if (moon.getPosition().getX() > other.getPosition().getX()) {
                    acceleration.setX(acceleration.getX() - 1);
                }

                if (moon.getPosition().getY() < other.getPosition().getY()) {
                    acceleration.setY(acceleration.getY() + 1);
                } else if (moon.getPosition().getY() > other.getPosition().getY()) {
                    acceleration.setY(acceleration.getY() - 1);
                }

                if (moon.getPosition().getZ() < other.getPosition().getZ()) {
                    acceleration.setZ(acceleration.getZ() + 1);
                } else if (moon.getPosition().getZ() > other.getPosition().getZ()) {
                    acceleration.setZ(acceleration.getZ() - 1);
                }
            }
            moon.setVelocity(moon.getVelocity().add(acceleration));
        }
        for (var moon : moons) {
            moon.setPosition(moon.getPosition().add(moon.getVelocity()));
        }
    }

    public long getEnergy() {
        return moons.stream()
                .mapToLong(Moon::getEnergy)
                .sum();
    }

    public boolean isComponentEquals(MoonSystem other, Function<Vector, Long> extractor) {
        var res = true;
        for (var i = 0; i < moons.size(); i++) {
            if (!this.getMoons().get(i)
                    .isComponentEquals(other.getMoons().get(i), extractor)) {
                res = false;
                break;
            }
        }
        return res;
    }

    @Override
    public String toString() {
        return moons.stream()
                .map(Moon::toString)
                .collect(Collectors.joining("\n"));
    }
}
