package com.leroy.ronan.aoc2019.day12;

import java.util.Objects;

public class Vector {

    private long x;
    private long y;
    private long z;

    public Vector(String data) {
        for (var coords :data.substring(1, data.length()-1)
                .split(",")) {
            var coord = coords.split("=");
            if ("x".equals(coord[0].trim())) {
                this.x = Long.valueOf(coord[1].trim());
            } else if ("y".equals(coord[0].trim())) {
                this.y = Long.valueOf(coord[1].trim());
            } else if ("z".equals(coord[0].trim())) {
                this.z = Long.valueOf(coord[1].trim());
            }
        }
    }

    public Vector(long x, long y, long z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public long getX() {
        return x;
    }

    public void setX(long x) {
        this.x = x;
    }

    public long getY() {
        return y;
    }

    public void setY(long y) {
        this.y = y;
    }

    public long getZ() {
        return z;
    }

    public void setZ(long z) {
        this.z = z;
    }

    public Vector add(Vector that) {
        return new Vector(
                this.x + that.x,
                this.y + that.y,
                this.z + that.z
        );
    }

    public long getEnergy() {
        return Math.abs(x)+Math.abs(y)+Math.abs(z);
    }

    public long getPPCM() {
        return ppcm(x, ppcm(y, z));
    }

    private static long ppcm(long nb1, long nb2) {
        long product, remain, ppcm;

        product = nb1*nb2;
        remain = nb1%nb2;
        while(remain != 0){
            nb1 = nb2;
            nb2 = remain;
            remain = nb1%nb2;
        }
        ppcm = product/nb2;
        //		System.out.println("PGCD = " + Nb2 + " PPCM = " + PPCM);
        return ppcm;
    }


    @Override
    public String toString() {
        return "<x="+x+",y="+y+",z="+z+">";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vector vector = (Vector) o;
        return x == vector.x &&
                y == vector.y &&
                z == vector.z;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y, z);
    }
}
