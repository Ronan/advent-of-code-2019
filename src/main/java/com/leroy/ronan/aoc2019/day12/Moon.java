package com.leroy.ronan.aoc2019.day12;

import java.util.function.Function;

public class Moon {

    private Vector position;
    private Vector velocity;

    public Moon(String data) {
        this.position = new Vector(data);
        this.velocity = new Vector(0,0,0);
    }

    public Vector getPosition() {
        return position;
    }

    public void setPosition(Vector position) {
        this.position = position;
    }

    public Vector getVelocity() {
        return velocity;
    }

    public void setVelocity(Vector velocity) {
        this.velocity = velocity;
    }

    public long getEnergy() {
        return position.getEnergy() * velocity.getEnergy();
    }

    public boolean isComponentEquals(Moon other, Function<Vector, Long> extractor) {
        return extractor.apply(this.getPosition()).equals(extractor.apply(other.getPosition()))
            && extractor.apply(this.getVelocity()).equals(extractor.apply(other.getVelocity()));
    }

    @Override
    public String toString() {
        return "pos="+position.toString()+",vel="+velocity.toString();
    }

}
