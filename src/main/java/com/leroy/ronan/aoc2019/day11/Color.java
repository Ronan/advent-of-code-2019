package com.leroy.ronan.aoc2019.day11;

public enum Color {
    WHITE(1),
    BLACK(0),
    ;

    private int code;

    Color(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
