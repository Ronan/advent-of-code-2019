package com.leroy.ronan.aoc2019.day11;

import java.util.Objects;

public class Panel {
    private int x;
    private int y;

    public Panel(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @Override
    public String toString() {
        return "Panel{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Panel panel = (Panel) o;
        return x == panel.x &&
                y == panel.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }
}
