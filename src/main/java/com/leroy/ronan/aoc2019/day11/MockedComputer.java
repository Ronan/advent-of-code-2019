package com.leroy.ronan.aoc2019.day11;

import java.util.ArrayList;
import java.util.List;

public class MockedComputer implements Computer {

    private List<RobotInstruction> responses;

    public MockedComputer(List<RobotInstruction> responses) {
        this.responses = new ArrayList<>(responses);
    }

    @Override
    public void push(long code) {
    }

    @Override
    public RobotInstruction popInstructions() {
        var res = responses.get(0);
        responses.remove(0);
        return res;
    }

    @Override
    public boolean isHalted() {
        return responses.isEmpty();
    }

    @Override
    public void run() {

    }

}
