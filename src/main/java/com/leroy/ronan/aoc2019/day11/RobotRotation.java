package com.leroy.ronan.aoc2019.day11;

public enum RobotRotation {
    LEFT(0),
    RIGHT(1),
    ;

    private int code;

    RobotRotation(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
