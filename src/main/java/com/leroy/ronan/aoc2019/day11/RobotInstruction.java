package com.leroy.ronan.aoc2019.day11;

public class RobotInstruction {

    private Color color;
    private RobotRotation turn;

    public RobotInstruction(int color, int turn) {
        this.color = Color.BLACK;
        if (color == 1) {
            this.color = Color.WHITE;
        }
        this.turn = RobotRotation.LEFT;
        if (turn == 1) {
            this.turn = RobotRotation.RIGHT;
        }
    }

    public Color getColor() {
        return this.color;
    }

    public RobotRotation getTurn() {
        return this.turn;
    }

    @Override
    public String toString() {
        return "RobotInstruction{" +
                "color=" + color +
                ", turn=" + turn +
                '}';
    }
}
