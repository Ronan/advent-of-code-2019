package com.leroy.ronan.aoc2019.day11;


import java.util.HashSet;
import java.util.Set;

import static com.leroy.ronan.aoc2019.day11.RobotDirection.UP;
import static com.leroy.ronan.aoc2019.day11.RobotRotation.LEFT;

public class HullPaintingRobot {

    private Computer brain;
    private int x;
    private int y;
    private RobotDirection direction;
    private Set<Panel> whitePanels;
    private Set<Panel> paintedPanels;
    private int minY = -2;
    private int maxY = 2;
    private int minX = -2;
    private int maxX = 2;

    public HullPaintingRobot(Computer brain) {
        this.brain = brain;
        this.x = 0;
        this.y = 0;
        this.direction = UP;
        this.whitePanels = new HashSet<>();
        this.paintedPanels = new HashSet<>();
    }

    public void makeStep() {
        log(getColor().toString());
        brain.push(getColor().getCode());
        brain.run();
        RobotInstruction instruction = brain.popInstructions();
        log(instruction.toString());
        paint(new Panel(x, y), instruction.getColor());
        if (LEFT == instruction.getTurn()) {
            turnLeft();
        } else {
            turnRight();
        }
        forward();
        log("==========");
        log(this.getStatus());
        log("==========");
    }

    public void paint(Panel panel, Color color) {
        paintedPanels.add(panel);
        if (Color.WHITE == color) {
            whitePanels.add(panel);
        } else {
            whitePanels.remove(panel);
        }
    }

    private void forward() {
        switch (direction) {
            case UP:
                this.y--;
                break;
            case RIGHT:
                this.x++;
                break;
            case DOWN:
                this.y++;
                break;
            case LEFT:
                this.x--;
                break;
        }
        if (this.x < this.minX) {
            this.minX = x;
        }
        if (this.y < this.minY) {
            this.minY = y;
        }

        if (this.maxX < this.x) {
            this.maxX = x;
        }
        if (this.maxY < this.y) {
            this.maxY = y;
        }

    }

    private void turnLeft() {
        switch (direction) {
            case UP:
                this.direction = RobotDirection.LEFT;
                break;
            case RIGHT:
                this.direction = RobotDirection.UP;
                break;
            case DOWN:
                this.direction = RobotDirection.RIGHT;
                break;
            case LEFT:
                this.direction = RobotDirection.DOWN;
                break;
        }
    }

    private void turnRight() {
        switch (direction) {
            case UP:
                this.direction = RobotDirection.RIGHT;
                break;
            case RIGHT:
                this.direction = RobotDirection.DOWN;
                break;
            case DOWN:
                this.direction = RobotDirection.LEFT;
                break;
            case LEFT:
                this.direction = RobotDirection.UP;
                break;
        }
    }

    private Color getColor() {
        var res = Color.BLACK;
        if (whitePanels.contains(new Panel(this.x, this.y))) {
            res = Color.WHITE;
        }
        return res;
    }

    public String getStatus() {
        String res = "";
        for(int y = minY; y <= maxY; y++) {
            for (int x = minX; x <= maxX; x++) {
                if (x == this.x && y == this.y) {
                    res += this.direction.toString();
                } else if (this.whitePanels.contains(new Panel(x, y))) {
                    res += "#";
                } else {
                    res += ".";
                }
            }
            res += "\n";
        }
        return res;
    }

    public boolean halt() {
        return brain.isHalted();
    }

    public int countPaintedPanels() {
        return this.paintedPanels.size();
    }

    private void log(String message) {
        // System.out.println(message);
    }
}
