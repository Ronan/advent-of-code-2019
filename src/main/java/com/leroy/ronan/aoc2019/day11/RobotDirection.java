package com.leroy.ronan.aoc2019.day11;

public enum RobotDirection {
    UP("^"),
    RIGHT(">"),
    DOWN("v"),
    LEFT("<"),
    ;

    private String display;

    RobotDirection(String display) {
        this.display = display;
    }

    @Override
    public String toString() {
        return display;
    }
}
