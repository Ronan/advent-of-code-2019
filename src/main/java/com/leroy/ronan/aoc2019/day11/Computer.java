package com.leroy.ronan.aoc2019.day11;

public interface Computer {
    void push(long code);

    RobotInstruction popInstructions();

    boolean isHalted();

    void run();
}
