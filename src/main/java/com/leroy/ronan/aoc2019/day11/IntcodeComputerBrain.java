package com.leroy.ronan.aoc2019.day11;

import com.leroy.ronan.aoc2019.IntcodeComputer;

public class IntcodeComputerBrain implements Computer {
    private IntcodeComputer computer;

    public IntcodeComputerBrain(IntcodeComputer computer) {
        this.computer = computer;
    }

    @Override
    public void push(long code) {
        this.computer.pushInput(code);
    }

    @Override
    public RobotInstruction popInstructions() {
        long[] output = this.computer.popOutput();
        return new RobotInstruction((int)output[0], (int)output[1]);
    }

    @Override
    public boolean isHalted() {
        return this.computer.isHalted();
    }

    @Override
    public void run() {
        this.computer.run();
    }
}
