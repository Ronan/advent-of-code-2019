package com.leroy.ronan.aoc2019;

public class OpcodeParameter {

    private long address;
    private long value;

    public OpcodeParameter(long address, long value) {
        this.address = address;
        this.value = value;
    }

    public long getAddress() {
        return address;
    }

    public long getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "OpcodeParameter{" +
                "address=" + address +
                ", value=" + value +
                '}';
    }
}
