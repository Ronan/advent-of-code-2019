package com.leroy.ronan.aoc2019.day13;

import com.leroy.ronan.aoc2019.Point;
import java.util.Map;

public class Player {

    private Map<Point, GameContent> board;

    public Player() {
    }

    public long get() {
        if (board == null) {
            return 0;
        }
        var ball = board.entrySet().stream()
                .filter(entry -> entry.getValue() == GameContent.BALL)
                .map(Map.Entry::getKey)
                .findFirst().orElse(null);
        var paddle = board.entrySet().stream()
                .filter(entry -> entry.getValue() == GameContent.PADDLE)
                .map(Map.Entry::getKey)
                .findFirst().orElse(null);

        if (ball.getX() < paddle.getX()) {
            return -1;
        } else if (ball.getX() > paddle.getX()) {
            return 1;
        } else {
            return 0;
        }
    }

    public void setBoard(Map<Point, GameContent> board) {
        this.board = board;
    }
}
