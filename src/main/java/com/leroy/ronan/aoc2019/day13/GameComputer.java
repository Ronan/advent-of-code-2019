package com.leroy.ronan.aoc2019.day13;

import com.leroy.ronan.aoc2019.IntcodeComputer;
import com.leroy.ronan.aoc2019.Point;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

public class GameComputer {

    private IntcodeComputer computer;
    private Map<Point, GameContent> board;
    private long score;
    private Player player;

    public GameComputer(String program, Player player) throws IOException, URISyntaxException {
        computer = new IntcodeComputer();
        computer.loadFromFile(program);
        board = new HashMap<>();
        this.player = player;
    }

    public void run() {
        while (!computer.isHalted()) {
            computer.pushInput(player.get());
            computer.run();
            updateBoard();
            player.setBoard(this.board);
            //System.out.println(this.toString());
        }
    }

    private void updateBoard() {
        var output = computer.popOutput();
        for (int i = 0; i < output.length; i = i+3) {
            var p = new Point((int) output[i], (int) output[i + 1]);
            if (new Point(-1, 0).equals(p)) {
                score = output[i+2];
            } else {
                board.put(p, GameContent.getByCode((int) output[i + 2]));
            }
        }
    }

    public void insertCoin() {
        computer.set(0, 2);
    }

    public boolean isHalted() {
        return computer.isHalted();
    }

    public long getBlockCount() {
        return board.values().stream()
                .filter(GameContent.BLOCK::equals).count();
    }

    public long getScore() {
        return this.score;
    }

    public String toString() {
        int maxX = -1;
        int maxY = -1;
        for (var p : board.keySet()) {
            if (p.getX() > maxX) {
                maxX = p.getX();
            }
            if (p.getY() >  maxY) {
                maxY = p.getY();
            }
        }
        StringBuilder all = new StringBuilder();
        for (int y = 0; y <= maxY; y++) {
            StringBuilder line = new StringBuilder();
            for (int x = 0; x <= maxX; x++) {
                line.append(board.get(new Point(x, y)).toString());
            }
            all.append(line).append("\n");
        }
        all.append("Blocks : " + this.getBlockCount() + " - Score : " + this.getScore());
        return all.toString();
    }
}
