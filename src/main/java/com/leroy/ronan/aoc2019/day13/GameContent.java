package com.leroy.ronan.aoc2019.day13;

import java.util.Arrays;

public enum GameContent {
    EMPTY(0, " "),
    WALL(1, "#"),
    BLOCK(2, "X"),
    PADDLE(3, "_"),
    BALL(4, "o"),
    ;

    private int code;
    private String value;

    GameContent(int code, String value) {
        this.code = code;
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }

    public static GameContent getByCode(int code) {
        return Arrays.stream(values())
                .filter(content -> content.code == code)
                .findFirst()
                .orElse(null);
    }
}
