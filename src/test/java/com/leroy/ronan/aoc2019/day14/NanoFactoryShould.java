package com.leroy.ronan.aoc2019.day14;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

public class NanoFactoryShould {

    @Test
    public void require_31_ore_for_1_fuel() {
        var factory = new NanoFactory(List.of(
                "10 ORE => 10 A",
                "1 ORE => 1 B",
                "7 A, 1 B => 1 C",
                "7 A, 1 C => 1 D",
                "7 A, 1 D => 1 E",
                "7 A, 1 E => 1 FUEL"
        ));
        Assertions.assertEquals(10, factory.getOreRequirement(10, "A"));
        Assertions.assertEquals(1, factory.getOreRequirement(1, "B"));
        Assertions.assertEquals(10, factory.getOreRequirement(7, "A"));
        Assertions.assertEquals(11, factory.getOreRequirement(1, "C"));
        Assertions.assertEquals(31, factory.getOreRequirement(1, "FUEL"));
        Assertions.assertEquals(
                new Reaction(
                        Map.of("ORE", 10L),
                        Map.of("A", 10L)
                ),
                factory.getReactionProducing("ORE", 1, "A")
        );
        Assertions.assertEquals(
                new Reaction(
                        Map.of("ORE", 11L),
                        Map.of("A", 3L, "C", 1L)
                ),
                factory.getReactionProducing("ORE", 1, "C")
        );
        Assertions.assertEquals(
                new Reaction(
                        Map.of("ORE", 21L),
                        Map.of("A", 6L, "D", 1L)
                ),
                factory.getReactionProducing("ORE", 1, "D")
        );
        Assertions.assertEquals(
                new Reaction(
                        Map.of("ORE", 31L),
                        Map.of("A", 9L, "E", 1L)
                ),
                factory.getReactionProducing("ORE", 1, "E")
        );
        Assertions.assertEquals(
                new Reaction(
                        Map.of("ORE", 31L),
                        Map.of("A", 2L, "FUEL", 1L)
                ),
                factory.getReactionProducing("ORE", 1, "FUEL")
        );
    }

    @Test
    public void require_165_ORE_with_second_sample_factory() {
        var factory = new NanoFactory(List.of(
                "9 ORE => 2 A",
                "8 ORE => 3 B",
                "7 ORE => 5 C",
                "3 A, 4 B => 1 AB",
                "5 B, 7 C => 1 BC",
                "4 C, 1 A => 1 CA",
                "2 AB, 3 BC, 4 CA => 1 FUEL"
        ));
        Assertions.assertEquals(165, factory.getOreRequirement(1, "FUEL"));
    }

    @Test
    public void require_13312_ORE_with_third_sample_factory() {
        var factory = new NanoFactory(List.of(
                "157 ORE => 5 NZVS",
                "165 ORE => 6 DCFZ",
                "44 XJWVT, 5 KHKGT, 1 QDVJ, 29 NZVS, 9 GPVTF, 48 HKGWZ => 1 FUEL",
                "12 HKGWZ, 1 GPVTF, 8 PSHF => 9 QDVJ",
                "179 ORE => 7 PSHF",
                "177 ORE => 5 HKGWZ",
                "7 DCFZ, 7 PSHF => 2 XJWVT",
                "165 ORE => 2 GPVTF",
                "3 DCFZ, 7 NZVS, 5 HKGWZ, 10 PSHF => 8 KHKGT"
        ));
        Assertions.assertEquals(13312, factory.getOreRequirement(1, "FUEL"));
        Assertions.assertEquals(
                new Reaction(
                        Map.of("ORE", 157L),
                        Map.of("NZVS", 5L)
                ),
                factory.getReactionProducing("ORE", 5, "NZVS")
        );

        Assertions.assertEquals(
                new Reaction(
                        Map.of("ORE", 314L),
                        Map.of("NZVS", 10L)
                ),
                factory.getReactionProducing("ORE", 10, "NZVS")
        );

        Assertions.assertEquals(
                new Reaction(
                        Map.of("ORE", 157L * 3),
                        Map.of("NZVS", 15L)
                ),
                factory.getReactionProducing("ORE", 15, "NZVS")
        );

        Assertions.assertEquals(
                new Reaction(
                        Map.of("ORE", 157L * 3),
                        Map.of("NZVS", 15L)
                ),
                factory.getReactionProducing("ORE", 14, "NZVS")
        );

        Assertions.assertEquals(
                new Reaction(
                        Map.of("ORE", 157L),
                        Map.of("NZVS", 5L)
                ),
                factory.getReactionProducing(1, "NZVS")
        );

        Assertions.assertEquals(
                new Reaction(
                        Map.of("ORE", 157L),
                        Map.of("NZVS", 5L)
                ),
                factory.getReactionProducing(5, "NZVS")
        );

        Assertions.assertEquals(
                new Reaction(
                        Map.of("ORE", 314L),
                        Map.of("NZVS", 10L)
                ),
                factory.getReactionProducing(10, "NZVS")
        );

        Assertions.assertEquals(
                new Reaction(
                        Map.of("ORE", 157L * 3),
                        Map.of("NZVS", 15L)
                ),
                factory.getReactionProducing(15, "NZVS")
        );

        Assertions.assertEquals(
                new Reaction(
                        Map.of("ORE", 157L * 3),
                        Map.of("NZVS", 15L)
                ),
                factory.getReactionProducing(14, "NZVS")
        );

        Assertions.assertEquals(82892753, factory.getMaxFuelFor(1000000000000L));
    }

    @Test
    public void require_180697_ORE_with_fourth_sample_factory() {
        var factory = new NanoFactory(List.of(
                "2 VPVL, 7 FWMGM, 2 CXFTF, 11 MNCFX => 1 STKFG",
                "17 NVRVD, 3 JNWZP => 8 VPVL",
                "53 STKFG, 6 MNCFX, 46 VJHF, 81 HVMC, 68 CXFTF, 25 GNMV => 1 FUEL",
                "22 VJHF, 37 MNCFX => 5 FWMGM",
                "139 ORE => 4 NVRVD",
                "144 ORE => 7 JNWZP",
                "5 MNCFX, 7 RFSQX, 2 FWMGM, 2 VPVL, 19 CXFTF => 3 HVMC",
                "5 VJHF, 7 MNCFX, 9 VPVL, 37 CXFTF => 6 GNMV",
                "145 ORE => 6 MNCFX",
                "1 NVRVD => 8 CXFTF",
                "1 VJHF, 6 MNCFX => 4 RFSQX",
                "176 ORE => 6 VJHF"
        ));
        Assertions.assertEquals(180697, factory.getOreRequirement(1, "FUEL"));
        Assertions.assertEquals(5586022, factory.getMaxFuelFor(1000000000000L));
    }

    @Test
    public void require_337075_ORE_with_real_factory() throws IOException, URISyntaxException {
        var factory = new NanoFactory("day-14-input.txt");
        Assertions.assertEquals(337075, factory.getOreRequirement(1, "FUEL"));
        Assertions.assertEquals(5194174, factory.getMaxFuelFor(1000000000000L));
    }
}
