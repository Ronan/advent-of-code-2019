package com.leroy.ronan.aoc2019.day4;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class SecureContainerShould {

    @Test
    public void accept_111111() {
        var container = new SecureContainer();
        Assertions.assertFalse(container.accepts(111111));
    }

    @Test
    public void refuse_223450() {
        var container = new SecureContainer();
        Assertions.assertFalse(container.accepts(223450));
    }

    @Test
    public void refuse_123789() {
        var container = new SecureContainer();
        Assertions.assertFalse(container.accepts(123789));
    }

    @Test
    public void count_acceptable() {
        var container = new SecureContainer();

        int count = 0;
        for (int i = 136818; i < 685979; i++) {
            if (container.accepts(i)) {
                count++;
            }
        }
        Assertions.assertEquals(1291, count);
    }

    @Test
    public void accept_112233() {
        var container = new SecureContainer();
        Assertions.assertTrue(container.accepts(112233));
    }

    @Test
    public void refuse_123444() {
        var container = new SecureContainer();
        Assertions.assertFalse(container.accepts(123444));
    }

    @Test
    public void accept_111122() {
        var container = new SecureContainer();
        Assertions.assertTrue(container.accepts(111122));
    }

}
