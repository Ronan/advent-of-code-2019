package com.leroy.ronan.aoc2019.day7;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URISyntaxException;

public class ThrusterAmplifierShould {

    @Test
    public void amplify_sample_1_into_43210() {
        var thruster = new ThrusterAmplifier(
                new long[]{3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0}
        );
        Assertions.assertArrayEquals(new long[]{4,3,2,1,0}, thruster.search(new long[]{0,1,2,3,4}));
        var actual = thruster.amplify(new long[]{4,3,2,1,0});
        Assertions.assertEquals(43210, actual);
    }

    @Test
    public void amplify_sample_2_into_54321() {
        var thruster = new ThrusterAmplifier(new long[]{
                3,23,3,24,1002,24,10,24,1002,23,-1,23,
                101,5,23,23,1,24,23,23,4,23,99,0,0
        });
        Assertions.assertArrayEquals(new long[]{0,1,2,3,4}, thruster.search(new long[]{0,1,2,3,4}));
        var actual = thruster.amplify(new long[]{0,1,2,3,4});
        Assertions.assertEquals(54321, actual);
    }

    @Test
    public void amplify_sample_3_into_65210() {
        var thruster = new ThrusterAmplifier(new long[]{
                3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,
                1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0
        });
        Assertions.assertArrayEquals(new long[]{1,0,4,3,2}, thruster.search(new long[]{0,1,2,3,4}));
        var actual = thruster.amplify(new long[]{1,0,4,3,2});
        Assertions.assertEquals(65210, actual);
    }

    @Test
    public void amplify_program() throws IOException, URISyntaxException {
        var thruster = new ThrusterAmplifier("day-7-input.txt");
        Assertions.assertArrayEquals(new long[]{2,1,3,4,0}, thruster.search(new long[]{0,1,2,3,4}));
        var actual = thruster.amplify(new long[]{2,1,3,4,0});
        Assertions.assertEquals(17790, actual);
    }

    @Test
    public void loop_amplify_program_sample_1() {
        var thruster = new ThrusterAmplifier(new long[]{
                3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,
                27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5
        });
        Assertions.assertEquals(139629729, thruster.loopAmplify(new long[]{9,8,7,6,5}));
        Assertions.assertArrayEquals(new long[]{9,8,7,6,5}, thruster.search(new long[]{9,8,7,6,5}, true));
    }

    @Test
    public void loop_amplify_program_sample_2() {
        var thruster = new ThrusterAmplifier(new long[]{
                3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,54,
                -5,54,1105,1,12,1,53,54,53,1008,54,0,55,1001,55,1,55,2,53,55,53,4,
                53,1001,56,-1,56,1005,56,6,99,0,0,0,0,10
        });
        Assertions.assertEquals(18216, thruster.loopAmplify(new long[]{9,7,8,5,6}));
        Assertions.assertArrayEquals(new long[]{9,7,8,5,6}, thruster.search(new long[]{9,8,7,6,5}, true));
    }

    @Test
    public void loop_amplify_program() throws IOException, URISyntaxException {
        var thruster = new ThrusterAmplifier("day-7-input.txt");
        Assertions.assertEquals(19384820, thruster.loopAmplify(new long[]{7,6,8,9,5}));
        Assertions.assertArrayEquals(new long[]{7,6,8,9,5}, thruster.search(new long[]{9,8,7,6,5}, true));
    }


}
