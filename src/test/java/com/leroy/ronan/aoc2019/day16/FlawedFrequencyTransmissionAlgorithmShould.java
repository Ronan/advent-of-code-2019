package com.leroy.ronan.aoc2019.day16;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class FlawedFrequencyTransmissionAlgorithmShould {

    private static FlawedFrequencyTransmissionAlgorithm fft;

    @BeforeAll
    static void setup() {
        fft = new FlawedFrequencyTransmissionAlgorithm();
    }

    @Test
    public void transform_12345678_into_48226158() {
        var output = fft.transform("12345678");
        Assertions.assertEquals("48226158", output);
    }

    @Test
    public void transform_48226158_into_34040438() {
        var output = fft.transform("48226158");
        Assertions.assertEquals("34040438", output);
    }

    @Test
    public void transform_with_2_phases_12345678_into_34040438() {
        var output = fft.transform("12345678", 2, 0);
        Assertions.assertEquals("34040438", output);
    }

    @Test
    public void apply_the_right_pattern_for_0() {
        Assertions.assertEquals(List.of(1, 0, -1, 0, 1, 0, -1, 0), this.getPattern(fft, 0, 8));

    }

    @Test
    public void apply_the_right_pattern_for_1() {
        Assertions.assertEquals(List.of(0, 1, 1, 0, 0, -1, -1, 0), this.getPattern(fft, 1, 8));
    }

    @Test
    public void apply_the_right_pattern_for_2() {
        Assertions.assertEquals(List.of(0, 0, 1, 1, 1, 0, 0, 0), this.getPattern(fft, 2, 8));
    }

    @Test
    public void apply_the_right_pattern_for_3() {
        Assertions.assertEquals(List.of(0, 0, 0, 1, 1, 1, 1, 0), this.getPattern(fft, 3, 8));
    }

    @Test
    public void apply_the_right_pattern_for_4() {
        Assertions.assertEquals(List.of(0, 0, 0, 0, 1, 1, 1, 1), this.getPattern(fft, 4, 8));
    }

    @Test
    public void apply_the_right_pattern_for_5() {
        Assertions.assertEquals(List.of(0, 0, 0, 0, 0, 1, 1, 1), this.getPattern(fft, 5, 8));
    }

    @Test
    public void apply_the_right_pattern_for_6() {
        Assertions.assertEquals(List.of(0, 0, 0, 0, 0, 0, 1, 1), this.getPattern(fft, 6, 8));
    }

    @Test
    public void apply_the_right_pattern_for_7() {
        Assertions.assertEquals(List.of(0, 0, 0, 0, 0, 0, 0, 1), this.getPattern(fft, 7, 8));
    }

    @Test
    public void apply_the_right_pattern_for_8() {
        Assertions.assertEquals(List.of(0, 0, 0, 0, 0, 0, 0, 0), this.getPattern(fft, 8, 8));
    }

    @Test
    public void transform_with_100_phases_80871224585914546619083218645595_into_24176176() {
        var output = fft.transform("80871224585914546619083218645595", 100, 0);
        Assertions.assertEquals("24176176", output.substring(0, 8));
    }

    @Test
    public void transform_with_100_phases_real_input_into_10189359() throws URISyntaxException, IOException {
        var path = Paths.get(getClass().getClassLoader().getResource("day-16-input.txt").toURI());
        String input = Files.readAllLines(path).stream().findFirst().orElse(null);
        var output = fft.transform(input, 100, 0);
        Assertions.assertEquals("10189359", output.substring(0, 8));
    }

    @Test
    public void transform_with_offset_in_100_phases_real_input_into_189359() throws URISyntaxException, IOException {
        var path = Paths.get(getClass().getClassLoader().getResource("day-16-input.txt").toURI());
        String input = Files.readAllLines(path).stream().findFirst().orElse(null);
        var output = fft.transform(input, 100, 2);
        Assertions.assertEquals("189359", output.substring(2, 8));
    }

    @Test
    public void transform_with_big_offset_in_100_phases_real_input_into_24176176() throws URISyntaxException, IOException {
        var path = Paths.get(getClass().getClassLoader().getResource("day-16-input.txt").toURI());
        String input = Files.readAllLines(path).stream().findFirst().orElse(null);
        var output = fft.transform(input, 100, 600);
        Assertions.assertEquals("1688858801", output.substring(600, 610));
    }

    @Test
    @Disabled
    public void transform_with_100_phases_big_real_input_into_24176176() throws URISyntaxException, IOException {
        var path = Paths.get(getClass().getClassLoader().getResource("day-16-input.txt").toURI());
        String input = Files.readAllLines(path).stream().findFirst().orElse(null);
        int offset = Integer.parseInt(input.substring(0, 7));
        String bigInput = IntStream.range(0, 10000)
                .mapToObj(i -> input)
                .collect(Collectors.joining());
        Assertions.assertEquals(5971313, offset);
        Assertions.assertEquals(6500000, bigInput.length());
        var output = fft.transform(bigInput, 100, offset);
        Assertions.assertEquals("80722126", output.substring(offset, offset+8));
    }

    public List<Integer> getPattern(FlawedFrequencyTransmissionAlgorithm fft, int line, int patternSize) {
        return IntStream.range(0, patternSize)
                .map(column-> fft.getPatternValue(column, line))
                .boxed()
                .collect(Collectors.toList());
    }
}
