package com.leroy.ronan.aoc2019.day6;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

public class LocalOrbitsMapShould {

    private static final String SAMPLE = "COM)B\n"+
            "B)C\n"+
            "C)D\n"+
            "D)E\n"+
            "E)F\n"+
            "B)G\n"+
            "G)H\n"+
            "D)I\n"+
            "E)J\n"+
            "J)K\n"+
            "K)L\n"+
            "K)YOU\n"+
            "I)SAN"
            ;

    @Test
    public void give_the_right_orbits_for_the_sample_map() {
        var map = new LocalOrbitsMap(SAMPLE);
        Assertions.assertEquals(0, map.getOrbitCount("COM"));
        Assertions.assertEquals(1, map.getOrbitCount("B"));
        Assertions.assertEquals(2, map.getOrbitCount("C"));
        Assertions.assertEquals(3, map.getOrbitCount("D"));
        Assertions.assertEquals(7, map.getOrbitCount("L"));
        Assertions.assertEquals(54, map.getChecksum());
        Assertions.assertEquals(
                List.of("COM"),
                map.getPathToCenter("COM")
        );
        Assertions.assertEquals(
                List.of("YOU", "K", "J", "E", "D", "C", "B", "COM"),
                map.getPathToCenter("YOU")
        );
        Assertions.assertEquals(
                List.of("D", "C", "B", "COM"),
                map.getCommonPathToCenter("YOU", "SAN")
        );
        Assertions.assertEquals(
                4,
                map.getMinimumOrbitalTransferNeeded("YOU", "SAN")
        );
    }

    @Test
    public void give_the_right_orbits_for_the_real_map() throws URISyntaxException, IOException {
        var path = Paths.get(getClass().getClassLoader().getResource("day-6-input.txt").toURI());
        var map = new LocalOrbitsMap(Files.lines(path).collect(Collectors.joining("\n")));
        Assertions.assertEquals(294191, map.getChecksum());
        Assertions.assertEquals(424, map.getMinimumOrbitalTransferNeeded("YOU", "SAN"));
    }
}
