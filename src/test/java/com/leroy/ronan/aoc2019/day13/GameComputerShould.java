package com.leroy.ronan.aoc2019.day13;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URISyntaxException;

public class GameComputerShould {

    @Test
    public void count_205_blocks_on_game_start() throws IOException, URISyntaxException {
        var game = new GameComputer("day-13-input.txt", new Player());
        game.run();
        Assertions.assertTrue(game.isHalted());
        Assertions.assertEquals(205, game.getBlockCount());
        Assertions.assertEquals(0, game.getScore());
    }

    @Test
    public void count_204_blocks_on_game_start_with_coin() throws IOException, URISyntaxException {
        var game = new GameComputer("day-13-input.txt", new Player());
        game.insertCoin();
        game.run();
        Assertions.assertTrue(game.isHalted());
        Assertions.assertEquals(0, game.getBlockCount());
        Assertions.assertEquals(10292, game.getScore());
    }
}
