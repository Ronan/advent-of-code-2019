package com.leroy.ronan.aoc2019.day10;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class AsteroidBeltShould {

    @Test
    public void analyse_sample1_belt() {
        var belt = new AsteroidBelt(
                List.of(
                        ".#..#",
                        ".....",
                        "#####",
                        "....#",
                        "...##"
                ));

        Assertions.assertEquals("" +
                        ".#..#\n" +
                        ".....\n" +
                        "#####\n" +
                        "....#\n" +
                        "...##",
                belt.toString()
        );
        Assertions.assertNotNull(belt.getAsteroidAt(3, 4).orElse(null));
        Assertions.assertNull(belt.getAsteroidAt(0, 0).orElse(null));
        Asteroid a22 = belt.getAsteroidAt(2, 2).orElse(null);
        Asteroid a10 = belt.getAsteroidAt(1, 0).orElse(null);
        Asteroid a34 = belt.getAsteroidAt(3, 4).orElse(null);
        Asteroid a40 = belt.getAsteroidAt(4, 0).orElse(null);
        Assertions.assertTrue(a22.isBlocking(a10, a34));
        Assertions.assertFalse(a22.isBlocking(a10, a40));
        Assertions.assertFalse(a10.isBlocking(a22, a34));

        Assertions.assertTrue(belt.areInView(a10, a22));
        Assertions.assertEquals(7, belt.getAsteroidsInViewOf(a10).size());
        Assertions.assertEquals(new Asteroid(3, 4), belt.getBestLocation());
    }

    @Test
    public void analyse_sample2_belt() {
        var belt = new AsteroidBelt(
                List.of(
                        "......#.#.",
                        "#..#.#....",
                        "..#######.",
                        ".#.#.###..",
                        ".#..#.....",
                        "..#....#.#",
                        "#..#....#.",
                        ".##.#..###",
                        "##...#..#.",
                        ".#....####"
                ));
        Assertions.assertEquals(new Asteroid(5, 8), belt.getBestLocation());
        Assertions.assertEquals(33, belt.getAsteroidsInViewOf(new Asteroid(5, 8)).size());
    }

    @Test
    public void analyse_sample3_belt() {
        var belt = new AsteroidBelt(
                List.of(
                        "#.#...#.#.",
                        ".###....#.",
                        ".#....#...",
                        "##.#.#.#.#",
                        "....#.#.#.",
                        ".##..###.#",
                        "..#...##..",
                        "..##....##",
                        "......#...",
                        ".####.###."
                ));
        Assertions.assertEquals(new Asteroid(1, 2), belt.getBestLocation());
        Assertions.assertEquals(35, belt.getAsteroidsInViewOf(new Asteroid(1, 2)).size());
    }

    @Test
    public void analyse_sample4_belt() {
        var belt = new AsteroidBelt(
                List.of(
                        ".#..#..###",
                        "####.###.#",
                        "....###.#.",
                        "..###.##.#",
                        "##.##.#.#.",
                        "....###..#",
                        "..#.#..#.#",
                        "#..#.#.###",
                        ".##...##.#",
                        ".....#.#.."
                ));
        Assertions.assertEquals(new Asteroid(6, 3), belt.getBestLocation());
        Assertions.assertEquals(41, belt.getAsteroidsInViewOf(new Asteroid(6, 3)).size());
    }

    @Test
    public void analyse_sample5_belt() {
        var belt = new AsteroidBelt(
                List.of(
                        ".#..##.###...#######",
                        "##.############..##.",
                        ".#.######.########.#",
                        ".###.#######.####.#.",
                        "#####.##.#.##.###.##",
                        "..#####..#.#########",
                        "####################",
                        "#.####....###.#.#.##",
                        "##.#################",
                        "#####.##.###..####..",
                        "..######..##.#######",
                        "####.##.####...##..#",
                        ".#####..#.######.###",
                        "##...#.##########...",
                        "#.##########.#######",
                        ".####.#.###.###.#.##",
                        "....##.##.###..#####",
                        ".#.#.###########.###",
                        "#.#.#.#####.####.###",
                        "###.##.####.##.#..##"
                ));
        Assertions.assertEquals(new Asteroid(11, 13), belt.getBestLocation());
        Assertions.assertEquals(210, belt.getAsteroidsInViewOf(new Asteroid(11, 13)).size());
    }

    @Test
    public void analyse_real_belt() throws URISyntaxException, IOException {
        var path = Paths.get(getClass().getClassLoader().getResource("day-10-input.txt").toURI());
        var lines = Files.lines(path);
        var belt = new AsteroidBelt(lines.collect(Collectors.toList()));
        Assertions.assertEquals(new Asteroid(30, 34), belt.getBestLocation());
        Assertions.assertEquals(344, belt.getAsteroidsInViewOf(new Asteroid(30, 34)).size());
    }

    @Test
    public void analyse_part_2_belt() {
        var belt = new AsteroidBelt(
                List.of(
                        ".#....#####...#..",
                        "##...##.#####..##",
                        "##...#...#.#####.",
                        "..#.....X...###..",
                        "..#.#.....#....##"
                ));
        Assertions.assertEquals(new Asteroid(8, 3), belt.getMonitoringStation());
        Assertions.assertEquals(30, belt.getAsteroidsInViewOf(belt.getMonitoringStation()).size());
        belt = belt.vaporize(belt.getAsteroidsInViewOf(belt.getMonitoringStation()));

        Assertions.assertEquals("" +
                        "........#.....#..\n" +
                        "..........#.....#\n" +
                        ".................\n" +
                        "........X....##..\n" +
                        ".................",
                belt.toString()
        );
    }

    @Test
    public void analyse_angles_north() {
        Assertions.assertEquals(
                Math.PI * 0 / 4,
                new Asteroid(0, -1).getAngleWith(new Asteroid(0, 0)),
                0.00001
        );
    }

    @Test
    public void analyse_angles_north_east() {
        Assertions.assertEquals(
                Math.PI * 1 / 4,
                new Asteroid(1, -1).getAngleWith(new Asteroid(0, 0)),
                0.00001
        );
    }

    @Test
    public void analyse_angles_east() {
        Assertions.assertEquals(
                Math.PI * 2 / 4,
                new Asteroid(1, 0).getAngleWith(new Asteroid(0, 0)),
                0.00001
        );
    }

    @Test
    public void analyse_angles_south_east() {
        Assertions.assertEquals(
                Math.PI * 3 / 4,
                new Asteroid(1, 1).getAngleWith(new Asteroid(0, 0)),
                0.00001
        );
    }

    @Test
    public void analyse_angles_south() {
        Assertions.assertEquals(
                Math.PI * 4 / 4,
                new Asteroid(0, 1).getAngleWith(new Asteroid(0, 0)),
                0.00001
        );
    }

    @Test
    public void analyse_angles_south_west() {
        Assertions.assertEquals(
                Math.PI * 5 / 4,
                new Asteroid(-1, 1).getAngleWith(new Asteroid(0, 0)),
                0.00001
        );
    }

    @Test
    public void analyse_angles_west() {
        Assertions.assertEquals(
                Math.PI * 6 / 4,
                new Asteroid(-1, 0).getAngleWith(new Asteroid(0, 0)),
                0.00001
        );
    }

    @Test
    public void analyse_angles_north_west() {
        Assertions.assertEquals(
                Math.PI * 7 / 4,
                new Asteroid(-1, -1).getAngleWith(new Asteroid(0, 0)),
                0.00001
        );
    }

    @Test
    public void analyse_sample1bis_belt() {
        var belt = new AsteroidBelt(
                List.of(
                        ".#..#",
                        ".....",
                        "#####",
                        "..X.#",
                        "...##"
                ));
        Assertions.assertEquals(
                List.of(
                        new Asteroid(2, 2),
                        new Asteroid(4, 0),
                        new Asteroid(3, 2),
                        new Asteroid(4, 2),
                        new Asteroid(4, 3),
                        new Asteroid(4, 4),
                        new Asteroid(3, 4),
                        new Asteroid(0, 2),
                        new Asteroid(1, 2),
                        new Asteroid(1, 0)
                ),
                belt.getAsteroidsInViewOf(belt.getMonitoringStation()).stream()
                        .sorted(Comparator.comparingDouble(a -> a.getAngleWith(belt.getMonitoringStation())))
                        .collect(Collectors.toList())
        );
    }

    @Test
    public void analyse_part_2_belt_vaporization_order() {
        var belt = new AsteroidBelt(
                List.of(
                        ".#....#####...#..",
                        "##...##.#####..##",
                        "##...#...#.#####.",
                        "..#.....X...###..",
                        "..#.#.....#....##"
                ));

        Assertions.assertTrue(
                new Asteroid(16,4).getAngleWith(belt.getMonitoringStation())
                < new Asteroid(10,4).getAngleWith(belt.getMonitoringStation())
        );


        Assertions.assertEquals(
                List.of(
                        new Asteroid(8,1),
                        new Asteroid(9,0),
                        new Asteroid(9,1),
                        new Asteroid(10,0),
                        new Asteroid(9,2),
                        new Asteroid(11,1),
                        new Asteroid(12,1),
                        new Asteroid(11,2),
                        new Asteroid(15,1),
                        new Asteroid(12,2),
                        new Asteroid(13,2),
                        new Asteroid(14,2),
                        new Asteroid(15,2),
                        new Asteroid(12,3),
                        new Asteroid(16,4),
                        new Asteroid(15,4),
                        new Asteroid(10,4),
                        new Asteroid(4,4),
                        new Asteroid(2,4),
                        new Asteroid(2,3),
                        new Asteroid(0,2),
                        new Asteroid(1,2),
                        new Asteroid(0,1),
                        new Asteroid(1,1),
                        new Asteroid(5,2),
                        new Asteroid(1,0),
                        new Asteroid(5,1),
                        new Asteroid(6,1),
                        new Asteroid(6,0),
                        new Asteroid(7,0)
                ),
                belt.getAsteroidsInViewOf(belt.getMonitoringStation()).stream()
                        .sorted(Comparator.comparingDouble(a -> a.getAngleWith(belt.getMonitoringStation())))
                        .collect(Collectors.toList())
        );
    }

    @Test
    public void analyse_sample5_bis_belt() {
        var belt = new AsteroidBelt(
                List.of(
                        ".#..##.###...#######",
                        "##.############..##.",
                        ".#.######.########.#",
                        ".###.#######.####.#.",
                        "#####.##.#.##.###.##",
                        "..#####..#.#########",
                        "####################",
                        "#.####....###.#.#.##",
                        "##.#################",
                        "#####.##.###..####..",
                        "..######..##.#######",
                        "####.##.####...##..#",
                        ".#####..#.######.###",
                        "##...#.####X#####...",
                        "#.##########.#######",
                        ".####.#.###.###.#.##",
                        "....##.##.###..#####",
                        ".#.#.###########.###",
                        "#.#.#.#####.####.###",
                        "###.##.####.##.#..##"
                ));
        Asteroid monitoringStation = belt.getMonitoringStation();
        Assertions.assertEquals(new Asteroid(11, 13), monitoringStation);
        Assertions.assertEquals(210, belt.getAsteroidsInViewOf(monitoringStation).size());

        var orderedAsteroids = belt.getAsteroidsInVaporizationOrder();
        Assertions.assertEquals(new Asteroid(8,2), orderedAsteroids.get(199));
        Assertions.assertEquals(new Asteroid(11,1), orderedAsteroids.get(298));
    }

    @Test
    public void analyse_real_belt_bis() throws URISyntaxException, IOException {
        var path = Paths.get(getClass().getClassLoader().getResource("day-10-input.txt").toURI());
        var lines = Files.lines(path);
        var belt = new AsteroidBelt(lines.collect(Collectors.toList()));
        Assertions.assertEquals(new Asteroid(30, 34), belt.getBestLocation());
        Assertions.assertEquals(344, belt.getAsteroidsInViewOf(new Asteroid(30, 34)).size());
        belt.setMonitoringStation(30, 34);
        Assertions.assertEquals(new Asteroid(27, 32), belt.getAsteroidsInVaporizationOrder().get(199));
    }


}