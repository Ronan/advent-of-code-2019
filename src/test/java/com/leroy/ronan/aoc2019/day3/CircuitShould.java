package com.leroy.ronan.aoc2019.day3;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

public class CircuitShould {

    @Test
    public void show_empty_circuit() {
        var circuit = new CircuitBuilder().build();
        Assertions.assertEquals(
                "" +
                        "...\n" +
                        ".o.\n" +
                        "...",
                circuit.toString()
        );
    }
    @Test
    public void show_circuit1() {
        var circuit = new CircuitBuilder()
                .addWire("R8")
                .build();
        Assertions.assertEquals(
                "" +
                        "...........\n" +
                        ".o--------.\n" +
                        "...........",
                circuit.toString()
        );
    }

    @Test
    public void show_circuit2() {
        var circuit = new CircuitBuilder()
                .addWire("R8,U5")
                .build();
        Assertions.assertEquals(
                "" +
                        "...........\n" +
                        ".........|.\n" +
                        ".........|.\n" +
                        ".........|.\n" +
                        ".........|.\n" +
                        ".........|.\n" +
                        ".o-------+.\n" +
                        "...........",
                circuit.toString()
        );
    }

    @Test
    public void show_circuit3() {
        var circuit = new CircuitBuilder()
                .addWire("R8,U5,L5,D3")
                .build();
        Assertions.assertEquals(
                "" +
                        "...........\n" +
                        "....+----+.\n" +
                        "....|....|.\n" +
                        "....|....|.\n" +
                        "....|....|.\n" +
                        ".........|.\n" +
                        ".o-------+.\n" +
                        "...........",
                circuit.toString()
        );
    }

    @Test
    public void show_sample_0_circuit() {
        var circuit = new CircuitBuilder()
                .addWire("R8,U5,L5,D3")
                .addWire("U7,R6,D4,L4")
                .build();

        Assertions.assertEquals(
                "" +
                        "...........\n" +
                        ".+-----+...\n" +
                        ".|.....|...\n" +
                        ".|..+--X-+.\n" +
                        ".|..|..|.|.\n" +
                        ".|.-X--+.|.\n" +
                        ".|..|....|.\n" +
                        ".|.......|.\n" +
                        ".o-------+.\n" +
                        "...........",
                circuit.toString()
        );
    }

    @Test
    public void find_the_closest_intersection_distance_for_sample_0() {
        var circuit = new CircuitBuilder()
                .addWire("R8,U5,L5,D3")
                .addWire("U7,R6,D4,L4")
                .build();
        var distance = circuit.getClosestIntersectionDistance();
        Assertions.assertEquals(6, distance);
    }

    @Test
    public void not_find_intersection_when_wire_1_sample1() {
        var circuit = new CircuitBuilder()
                .addWire("R75,D30,R83,U83,L12,D49,R71,U7,L72")
                .build();
        var distance = circuit.getClosestIntersectionDistance();
        Assertions.assertEquals(-1, distance);
    }

    @Test
    public void find_the_closest_intersection_distance_for_sample_1() {
        var circuit = new CircuitBuilder()
                .addWire("R75,D30,R83,U83,L12,D49,R71,U7,L72")
                .addWire("U62,R66,U55,R34,D71,R55,D58,R83")
                .build();
        var distance = circuit.getClosestIntersectionDistance();
        Assertions.assertEquals(159, distance);
    }

    @Test
    public void find_the_closest_intersection_distance_for_sample_2() {
        var circuit = new CircuitBuilder()
                .addWire("R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51")
                .addWire("U98,R91,D20,R16,D67,R40,U7,R15,U6,R7")
                .build();
        var distance = circuit.getClosestIntersectionDistance();
        Assertions.assertEquals(135, distance);
    }

    @Test
    public void day_3_response_1() throws IOException, URISyntaxException {
        var circuit = new CircuitBuilder()
                .fromFile("day-3-input.txt")
                .build();
        var distance = circuit.getClosestIntersectionDistance();
        Assertions.assertEquals(2129, distance);
    }

    @Test
    public void count_30_steps_to_closest_intersection_for_sample_0() {
        var circuit = new CircuitBuilder()
                .addWire("R8,U5,L5,D3")
                .addWire("U7,R6,D4,L4")
                .build();
        var steps = circuit.getStepsToClosestIntersection();
        Assertions.assertEquals(30, steps);
    }

    @Test
    public void count_610_steps_to_closest_intersection_for_sample_1() {
        var circuit = new CircuitBuilder()
                .addWire("R75,D30,R83,U83,L12,D49,R71,U7,L72")
                .addWire("U62,R66,U55,R34,D71,R55,D58,R83")
                .build();
        var steps = circuit.getStepsToClosestIntersection();
        Assertions.assertEquals(610, steps);
    }

    @Test
    public void count_610_steps_to_closest_intersection_for_sample_2() {
        var circuit = new CircuitBuilder()
                .addWire("R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51")
                .addWire("U98,R91,D20,R16,D67,R40,U7,R15,U6,R7")
                .build();
        var steps = circuit.getStepsToClosestIntersection();
        Assertions.assertEquals(410, steps);
    }

    @Test
    public void day_3_response_2() throws IOException, URISyntaxException {
        var circuit = new CircuitBuilder()
                .fromFile("day-3-input.txt")
                .build();
        var steps = circuit.getStepsToClosestIntersection();
        Assertions.assertEquals(134662, steps);
    }
}
