package com.leroy.ronan.aoc2019.day1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URISyntaxException;

public class FuelCounterUpperShould {

    @Test
    public void require_2_for_a_mass_of_12() {
        var fuelCounterUpper = new FuelCounterUpper();
        var requirement = fuelCounterUpper.getRequirementForMass(12);
        Assertions.assertEquals(2, requirement);
    }

    @Test
    public void require_2_for_a_mass_of_14() {
        var fuelCounterUpper = new FuelCounterUpper();
        var requirement = fuelCounterUpper.getRequirementForMass(14);
        Assertions.assertEquals(2, requirement);
    }

    @Test
    public void require_654_for_a_mass_of_1969() {
        var fuelCounterUpper = new FuelCounterUpper();
        var requirement = fuelCounterUpper.getRequirementForMass(1969);
        Assertions.assertEquals(654, requirement);
    }

    @Test
    public void require_33583_for_a_mass_of_100756() {
        var fuelCounterUpper = new FuelCounterUpper();
        var requirement = fuelCounterUpper.getRequirementForMass(100756);
        Assertions.assertEquals(33583, requirement);
    }

    @Test
    public void require_2_for_a_module_mass_of_14() {
        var fuelCounterUpper = new FuelCounterUpper();
        var requirement = fuelCounterUpper.getRequirementForModuleMass(14);
        Assertions.assertEquals(2, requirement);
    }

    @Test
    public void require_966_for_a_mass_of_1969() {
        var fuelCounterUpper = new FuelCounterUpper();
        var requirement = fuelCounterUpper.getRequirementForModuleMass(1969);
        Assertions.assertEquals(966, requirement);
    }

    @Test
    public void require_50346_for_a_mass_of_100756() {
        var fuelCounterUpper = new FuelCounterUpper();
        var requirement = fuelCounterUpper.getRequirementForModuleMass(100756);
        Assertions.assertEquals(50346, requirement);
    }

    @Test
    public void retrieve_requirement_for_all_modules() throws URISyntaxException, IOException {
        var fuelCounterUpper = new FuelCounterUpper();
        Assertions.assertEquals(4870838, fuelCounterUpper.getGlobalRequirement());
    }

}
