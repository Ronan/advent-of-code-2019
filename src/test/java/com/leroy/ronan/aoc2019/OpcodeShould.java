package com.leroy.ronan.aoc2019;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static com.leroy.ronan.aoc2019.OpcodeParameterMode.*;

public class OpcodeShould {

    @Test
    public void be_parsed_properly_for_21108() {
        var instruction = new Opcode(21108);
        Assertions.assertEquals("08", instruction.getOp());
        Assertions.assertEquals(DIRECT, instruction.getMode(1));
        Assertions.assertEquals(DIRECT, instruction.getMode(2));
        Assertions.assertEquals(RELATIVE, instruction.getMode(3));
    }
    @Test

    public void be_parsed_properly_for_01() {
        var instruction = new Opcode(1);
        Assertions.assertEquals("01", instruction.getOp());
        Assertions.assertEquals(POSITION, instruction.getMode(1));
        Assertions.assertEquals(POSITION, instruction.getMode(2));
        Assertions.assertEquals(POSITION, instruction.getMode(3));
    }
}
