package com.leroy.ronan.aoc2019.day2;

import com.leroy.ronan.aoc2019.IntcodeComputer;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URISyntaxException;

public class IntcodeComputerShould {

    @Test
    public void load_program() throws IOException, URISyntaxException {
        var program = new IntcodeComputer();
        program.loadFromFile("day-2-input.txt");
        var memory = program.getMemory();
        Assertions.assertEquals(1, memory[0]);
        Assertions.assertEquals(0, memory[1]);
        Assertions.assertEquals(0, memory[2]);
    }

    @Test
    public void activate_1202_alarm() throws IOException, URISyntaxException {
        var program = new IntcodeComputer();
        program.loadFromFile("day-2-input.txt");
        program.set(1, 12);
        program.set(2, 2);

        var memory = program.getMemory();

        Assertions.assertEquals(1, memory[0]);
        Assertions.assertEquals(12, memory[1]);
        Assertions.assertEquals(2, memory[2]);
    }

    @Test
    public void run_sample_1() {
        var program = new IntcodeComputer();
        program.load(new long[]{1, 0, 0, 0, 99});
        program.run();

        var memory = program.getMemory();

        Assertions.assertArrayEquals(new long[]{2, 0, 0, 0, 99}, memory);
    }

    @Test
    public void run_sample_2() {
        var program = new IntcodeComputer();
        program.load(new long[]{2, 3, 0, 3, 99});
        program.run();

        var memory = program.getMemory();

        Assertions.assertArrayEquals(new long[]{2, 3, 0, 6, 99}, memory);
    }

    @Test
    public void run_sample_3() {
        var program = new IntcodeComputer();
        program.load(new long[]{2, 4, 4, 5, 99, 0});
        program.run();

        var memory = program.getMemory();

        Assertions.assertArrayEquals(new long[]{2, 4, 4, 5, 99, 9801}, memory);
    }

    @Test
    public void run_sample_4() {
        var program = new IntcodeComputer();
        program.load(new long[]{1, 1, 1, 4, 99, 5, 6, 0, 99});
        program.run();

        var memory = program.getMemory();

        Assertions.assertArrayEquals(new long[]{30, 1, 1, 4, 2, 5, 6, 0, 99}, memory);
    }

    @Test
    public void run_gravity_assist() throws IOException, URISyntaxException {
        var program = new IntcodeComputer();
        program.loadFromFile("day-2-input.txt");
        program.set(1, 12);
        program.set(2, 2);
        program.run();

        var memory = program.getMemory();

        Assertions.assertEquals(165, memory.length);
        Assertions.assertEquals(4462686, memory[0]);
    }

    @Test
    public void run_gravity_assist2() throws IOException, URISyntaxException {
        var program = new IntcodeComputer();
        Assertions.assertEquals(4462686, program.runWithNounAndVerb("day-2-input.txt", 12, 2));
    }

    @Test
    public void run_programs() throws IOException, URISyntaxException {
        var program = new IntcodeComputer();
        var input = program.getInputForGivenOutput(19690720, "day-2-input.txt");
        Assertions.assertEquals(59, input[0]);
        Assertions.assertEquals(36, input[1]);

    }
}
