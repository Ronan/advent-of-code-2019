package com.leroy.ronan.aoc2019.day9;

import com.leroy.ronan.aoc2019.IntcodeComputer;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Arrays;

public class IncodeComputerShould {

    @Test
    public void write_it_self_to_output() {
        var program = new long[]{109, 1, 204, -1, 1001, 100, 1, 100, 1008, 100, 16, 101, 1006, 101, 0, 99};
        var computer = new IntcodeComputer();
        computer.load(program);
        computer.run();

        Assertions.assertArrayEquals(program, computer.getOutputArray());
    }

    @Test
    public void write_a_16_digits_number() {
        var program = new long[]{1102,34915192,34915192,7,4,7,99,0};
        var computer = new IntcodeComputer();
        computer.load(program);
        computer.run();

        Assertions.assertEquals(16, String.valueOf(computer.getOutput()).length());
    }

    @Test
    public void output_a_large_number() {
        var program = new long[]{104,1125899906842624L,99};
        var computer = new IntcodeComputer();
        computer.load(program);
        computer.run();

        Assertions.assertEquals(1125899906842624L, computer.getOutput());
    }

    @Test
    public void check_BOOST_program() throws IOException, URISyntaxException {
        var computer = new IntcodeComputer();
        computer.loadFromFile("day-9-input.txt");
        computer.pushInput(1L);
        computer.run();

        Assertions.assertArrayEquals(new long[]{2738720997L}, computer.getOutputArray(),
                "Got : " + Arrays.toString(computer.getOutputArray())
        );
    }

    @Test
    public void run_BOOST_program() throws IOException, URISyntaxException {
        var computer = new IntcodeComputer();
        computer.loadFromFile("day-9-input.txt");
        computer.pushInput(2L);
        computer.run();

        Assertions.assertArrayEquals(new long[]{50894L}, computer.getOutputArray(),
                "Got : " + Arrays.toString(computer.getOutputArray())
        );
    }

}
