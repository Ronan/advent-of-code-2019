package com.leroy.ronan.aoc2019.day15;

import java.util.ArrayList;
import java.util.List;

public class MockedDroidBrain implements DroidBrain {

    private List<Long> responses;

    public MockedDroidBrain(List<Long> responses) {
        this.responses = new ArrayList<>();
        this.responses.addAll(responses);
    }

    public void push(long value) {

    }

    public void run() {

    }

    public long pop() {
        var res = responses.get(0);
        responses.remove(0);
        return res;
    }
}
