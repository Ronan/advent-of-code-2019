package com.leroy.ronan.aoc2019.day15;

import com.leroy.ronan.aoc2019.IntcodeComputer;
import com.leroy.ronan.aoc2019.Point;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class RepairDroidShould {

    @Test
    public void output_initial_state() {
        var droid = new RepairDroid(null);

        String result;
        result = droid.getAreaMap().getMapAsString(false, true, droid.getLocation());
        Assertions.assertEquals("" +
                "   \n" +
                " D \n" +
                "   ",
                result);
    }

    @Test
    public void output_map_with_wall_on_north() {
        var brain = new MockedDroidBrain(List.of(0L));
        var droid = new RepairDroid(brain);
        droid.move(Direction.NORTH);
        String result;
        result = droid.getAreaMap().getMapAsString(false, true, droid.getLocation());
        Assertions.assertEquals("" +
                        "   \n" +
                        " # \n" +
                        " D \n" +
                        "   ",
                result);
    }

    @Test
    public void output_sample_map() {
        var brain = new MockedDroidBrain(List.of(0L, 1L, 0L, 0L, 0L, 1L, 0L, 1L, 0L, 2L));
        var droid = new RepairDroid(brain);
        droid.move(Direction.NORTH);
        droid.move(Direction.EAST);
        droid.move(Direction.NORTH);
        droid.move(Direction.SOUTH);
        droid.move(Direction.EAST);
        droid.move(Direction.WEST);
        droid.move(Direction.WEST);
        droid.move(Direction.SOUTH);
        droid.move(Direction.SOUTH);
        droid.move(Direction.WEST);
        String result;
        result = droid.getAreaMap().getMapAsString(false, true, droid.getLocation());
        Assertions.assertEquals("" +
                        "      \n" +
                        "  ##  \n" +
                        " #..# \n" +
                        " D.#  \n" +
                        "  #   \n" +
                        "      ",
                result);
    }

    @Test
    public void repair_area() throws IOException, URISyntaxException {
        var computer = new IntcodeComputer();
        computer.loadFromFile("day-15-input.txt");
        var brain = new IntcodeDroidBrain(computer);
        var droid = new RepairDroid(brain);

        List<Point> pathToTarget = droid.explore();

        Path mapPath = Paths.get(".").resolve("src").resolve("test").resolve("resources").resolve("day-15-map.txt");
        var expected = String.join("\n", Files.readAllLines(mapPath));

        Assertions.assertEquals(expected, droid.getAreaMap().getMapAsString(true, true, null));
        Assertions.assertEquals(413, pathToTarget.size()); // Response is 412.

        int timeToFill = droid.repair();
        Assertions.assertEquals(418, timeToFill);

    }
}
