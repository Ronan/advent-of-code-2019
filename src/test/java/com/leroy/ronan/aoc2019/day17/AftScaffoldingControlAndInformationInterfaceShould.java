package com.leroy.ronan.aoc2019.day17;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class AftScaffoldingControlAndInformationInterfaceShould {

	@Test
	public void run_program() throws IOException, URISyntaxException {
		var ascii = new AftScaffoldingControlAndInformationInterface();
		ascii.load();
		var image = ascii.getImage();

		var path = Paths.get(getClass().getClassLoader().getResource("day-17-image.txt").toURI());
		Assertions.assertEquals(
				String.join("\n", Files.readAllLines(path)),
				image
		);

		Assertions.assertEquals(41, ascii.getWidth());
		Assertions.assertEquals(49, ascii.getHeight());

		Assertions.assertEquals('#', ascii.charAt(0, 0));
		Assertions.assertEquals('.', ascii.charAt(1, 1));
		Assertions.assertEquals('#', ascii.charAt(6, 10));

		Assertions.assertFalse(ascii.isIntersect(0, 0));
		Assertions.assertFalse(ascii.isIntersect(1, 1));
		Assertions.assertTrue(ascii.isIntersect(6, 10));

		Assertions.assertEquals(0, ascii.getAlignmentParameter(0, 0));
		Assertions.assertEquals(0, ascii.getAlignmentParameter(1, 1));
		Assertions.assertEquals(60, ascii.getAlignmentParameter(6, 10));

		Assertions.assertEquals(6052, ascii.getAlignmentParameter());
	}
}
