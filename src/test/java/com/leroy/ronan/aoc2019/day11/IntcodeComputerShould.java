package com.leroy.ronan.aoc2019.day11;

import com.leroy.ronan.aoc2019.IntcodeComputer;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

public class IntcodeComputerShould {

    @Test
    public void run_day_11_program() throws IOException, URISyntaxException {
        var computer = new IntcodeComputer();
        computer.loadFromFile("day-11-input.txt");
        computer.run();
        Assertions.assertArrayEquals(new long[0], computer.getOutputArray());

        // at 0,0 -> black
        computer.pushInput(0);
        computer.run();
        var output = computer.popOutput();
        Assertions.assertArrayEquals(new long[]{1,0}, output, Arrays.toString(output));
        // paint white turn left

        // at 1,0 -> black
        computer.pushInput(0);
        computer.run();
        output = computer.popOutput();
        Assertions.assertArrayEquals(new long[]{1,0}, output, Arrays.toString(output));
        // paint white turn left
    }

    @Test
    public void make_first_step_with_sample_brain() {
        Computer brain = new MockedComputer(List.of(
                new RobotInstruction(1, 0)
        ));

        var robot = new HullPaintingRobot(brain);
        Assertions.assertEquals("" +
                ".....\n" +
                ".....\n" +
                "..^..\n" +
                ".....\n" +
                ".....\n",
                robot.getStatus());
        robot.makeStep();
        Assertions.assertEquals("" +
                        ".....\n" +
                        ".....\n" +
                        ".<#..\n" +
                        ".....\n" +
                        ".....\n",
                robot.getStatus());
    }

    @Test
    public void make_all_sample_steps() {
        Computer brain = new MockedComputer(List.of(
                new RobotInstruction(1, 0),
                new RobotInstruction(0, 0),
                new RobotInstruction(1, 0),
                new RobotInstruction(1, 0),
                new RobotInstruction(0, 1),
                new RobotInstruction(1, 0),
                new RobotInstruction(1, 0)
        ));

        var robot = new HullPaintingRobot(brain);
        Assertions.assertEquals("" +
                        ".....\n" +
                        ".....\n" +
                        "..^..\n" +
                        ".....\n" +
                        ".....\n",
                robot.getStatus());
        while (!robot.halt()) {
            robot.makeStep();
        }
        Assertions.assertEquals("" +
                        ".....\n" +
                        "..<#.\n" +
                        "...#.\n" +
                        ".##..\n" +
                        ".....\n",
                robot.getStatus());
        Assertions.assertEquals(6, robot.countPaintedPanels());
    }

    @Test
    public void run_real_program() throws IOException, URISyntaxException {
        var computer = new IntcodeComputer();
        computer.loadFromFile("day-11-input.txt");
        var brain = new IntcodeComputerBrain(computer);
        var robot = new HullPaintingRobot(brain);
        while(!robot.halt()) {
            robot.makeStep();
        }
        Assertions.assertEquals(2056, robot.countPaintedPanels());
    }

    @Test
    public void run_real_program_starting_on_white() throws IOException, URISyntaxException {
        var computer = new IntcodeComputer();
        computer.loadFromFile("day-11-input.txt");
        var brain = new IntcodeComputerBrain(computer);
        var robot = new HullPaintingRobot(brain);
        robot.paint(new Panel(0,0), Color.WHITE);
        while(!robot.halt()) {
            robot.makeStep();
        }
        Assertions.assertEquals(".............................................\n" +
                        ".............................................\n" +
                        "....##..#....###..####.###....##.####.###....\n" +
                        "...#..#.#....#..#.#....#..#....#....#.#..#...\n" +
                        "...#....#....###..###..#..#....#...#..#..#...\n" +
                        "...#.##.#....#..#.#....###.....#..#...###....\n" +
                        "...#..#.#....#..#.#....#....#..#.#....#....>.\n" +
                        "....###.####.###..####.#.....##..####.#......\n",
                robot.getStatus());
    }

}
