package com.leroy.ronan.aoc2019.day5;

import com.leroy.ronan.aoc2019.IntcodeComputer;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URISyntaxException;

public class IntcodeComputerShould {

    @Test
    public void load_program() throws IOException, URISyntaxException {
        var program = new IntcodeComputer();
        program.loadFromFile("day-2-input.txt");
        var memory = program.getMemory();
        Assertions.assertEquals(1, memory[0]);
        Assertions.assertEquals(0, memory[1]);
        Assertions.assertEquals(0, memory[2]);
    }

    @Test
    public void activate_1202_alarm() throws IOException, URISyntaxException {
        var program = new IntcodeComputer();
        program.loadFromFile("day-2-input.txt");
        program.set(1, 12);
        program.set(2, 2);

        var memory = program.getMemory();

        Assertions.assertEquals(1, memory[0]);
        Assertions.assertEquals(12, memory[1]);
        Assertions.assertEquals(2, memory[2]);
    }

    @Test
    public void run_sample_1() {
        var program = new IntcodeComputer();
        program.load(new long[]{1, 0, 0, 0, 99});
        program.run();

        var memory = program.getMemory();

        Assertions.assertArrayEquals(new long[]{2, 0, 0, 0, 99}, memory);
    }

    @Test
    public void run_sample_2() {
        var program = new IntcodeComputer();
        program.load(new long[]{2, 3, 0, 3, 99});
        program.run();

        var memory = program.getMemory();

        Assertions.assertArrayEquals(new long[]{2, 3, 0, 6, 99}, memory);
    }

    @Test
    public void run_sample_3() {
        var program = new IntcodeComputer();
        program.load(new long[]{2, 4, 4, 5, 99, 0});
        program.run();

        var memory = program.getMemory();

        Assertions.assertArrayEquals(new long[]{2, 4, 4, 5, 99, 9801}, memory);
    }

    @Test
    public void run_sample_4() {
        var program = new IntcodeComputer();
        program.load(new long[]{1, 1, 1, 4, 99, 5, 6, 0, 99});
        program.run();

        var memory = program.getMemory();

        Assertions.assertArrayEquals(new long[]{30, 1, 1, 4, 2, 5, 6, 0, 99}, memory);
    }

    @Test
    public void run_gravity_assist() throws IOException, URISyntaxException {
        var program = new IntcodeComputer();
        program.loadFromFile("day-2-input.txt");
        program.set(1, 12);
        program.set(2, 2);
        program.run();

        var memory = program.getMemory();

        Assertions.assertEquals(165, memory.length);
        Assertions.assertEquals(4462686, memory[0]);
    }

    @Test
    public void run_gravity_assist2() throws IOException, URISyntaxException {
        var program = new IntcodeComputer();
        Assertions.assertEquals(4462686, program.runWithNounAndVerb("day-2-input.txt", 12, 2));
    }

    @Test
    public void run_programs() throws IOException, URISyntaxException {
        var program = new IntcodeComputer();
        var input = program.getInputForGivenOutput(19690720, "day-2-input.txt");
        Assertions.assertEquals(59, input[0]);
        Assertions.assertEquals(36, input[1]);
    }

    @Test
    public void run_identity_program() {
        var program = new IntcodeComputer();
        program.load(new long[]{3,0,4,0,99});
        program.setInput(369852);
        program.run();
        Assertions.assertEquals(369852, program.getOutput());
    }

    @Test
    public void run_exit_program() {
        var program = new IntcodeComputer();
        program.load(new long[]{1002,4,3,4,33});
        program.run();
    }

    @Test
    public void run_with_negative_numbers() {
        var program = new IntcodeComputer();
        program.load(new long[]{1101,100,-1,4,0});
        program.run();
    }

    @Test
    public void run_TEST_program() throws IOException, URISyntaxException {
        var program = new IntcodeComputer();
        program.loadFromFile("day-5-input.txt");
        program.setInput(1);
        program.run();
        Assertions.assertEquals(7692125, program.getOutput());
    }

    @Test
    public void sample_compare_1_should_output_0_for_input_1() {
        var program = new IntcodeComputer();
        program.load(new long[]{3,9,8,9,10,9,4,9,99,-1,8});
        program.setInput(1);
        program.run();
        Assertions.assertEquals(0, program.getOutput());
    }

    @Test
    public void sample_compare_1_should_output_1_for_input_8() {
        var program = new IntcodeComputer();
        program.load(new long[]{3,9,8,9,10,9,4,9,99,-1,8});
        program.setInput(8);
        program.run();
        Assertions.assertEquals(1, program.getOutput());
    }

    @Test
    public void sample_compare_2_should_output_1_for_input_1() {
        var program = new IntcodeComputer();
        program.load(new long[]{3,9,7,9,10,9,4,9,99,-1,8});
        program.setInput(1);
        program.run();
        Assertions.assertEquals(1, program.getOutput());
    }

    @Test
    public void sample_compare_2_should_output_0_for_input_8() {
        var program = new IntcodeComputer();
        program.load(new long[]{3,9,7,9,10,9,4,9,99,-1,8});
        program.setInput(1);
        program.run();
        Assertions.assertEquals(1, program.getOutput());
    }

    @Test
    public void sample_compare_3_should_output_0_for_input_1() {
        var program = new IntcodeComputer();
        program.load(new long[]{3,3,1108,-1,8,3,4,3,99});
        program.setInput(1);
        program.run();
        Assertions.assertEquals(0, program.getOutput());
    }

    @Test
    public void sample_compare_3_should_output_1_for_input_8() {
        var program = new IntcodeComputer();
        program.load(new long[]{3,3,1108,-1,8,3,4,3,99});
        program.setInput(8);
        program.run();
        Assertions.assertEquals(1, program.getOutput());
    }

    @Test
    public void sample_compare_4_should_output_1_for_input_1() {
        var program = new IntcodeComputer();
        program.load(new long[]{3,3,1107,-1,8,3,4,3,99});
        program.setInput(1);
        program.run();
        Assertions.assertEquals(1, program.getOutput());
    }

    @Test
    public void sample_compare_4_should_output_0_for_input_8() {
        var program = new IntcodeComputer();
        program.load(new long[]{3,3,1107,-1,8,3,4,3,99});
        program.setInput(8);
        program.run();
        Assertions.assertEquals(0, program.getOutput());
    }

    @Test
    public void sample_jump_1_should_output_1_for_input_1() {
        var program = new IntcodeComputer();
        program.load(new long[]{3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9});
        program.setInput(1);
        program.run();
        Assertions.assertEquals(1, program.getOutput());
    }

    @Test
    public void sample_jump_1_should_output_0_for_input_0() {
        long[] source = {3, 12, 6, 12, 15, 1, 13, 14, 13, 4, 13, 99, -1, 0, 1, 9};
        long input = 0;

        var output = getProgramOutput(source, input);

        Assertions.assertEquals(0, output);
    }

    @Test
    public void sample_jump_2_should_output_1_for_input_1() {
        Assertions.assertEquals(1,
                getProgramOutput(
                        new long[]{3, 3, 1105, -1, 9, 1101, 0, 0, 12, 4, 12, 99, 1},
                        1
                )
        );
    }

    @Test
    public void sample_jump_2_should_output_0_for_input_0() {
        Assertions.assertEquals(0,
                getProgramOutput(
                        new long[]{3, 3, 1105, -1, 9, 1101, 0, 0, 12, 4, 12, 99, 1},
                        0
                )
        );
    }

    @Test
    public void verify_with_larger_jump_and_compare_example() {
        var program = new long[]{
                3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,
                1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,
                999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99
        };
        Assertions.assertEquals(999, getProgramOutput(program,0));
        Assertions.assertEquals(1000, getProgramOutput(program,8));
        Assertions.assertEquals(1001, getProgramOutput(program,9));
    }

    @Test
    public void run_TEST_program_with_input_5() throws IOException, URISyntaxException {
        var program = new IntcodeComputer();
        program.loadFromFile("day-5-input.txt");
        program.setInput(5);
        program.run();
        Assertions.assertEquals(14340395, program.getOutput());
    }

    private Long getProgramOutput(long[] source, long input) {
        var program = new IntcodeComputer();
        program.load(source);
        program.setInput(input);
        program.run();
        return program.getOutput();
    }
}
