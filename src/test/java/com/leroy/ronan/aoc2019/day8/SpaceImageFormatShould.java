package com.leroy.ronan.aoc2019.day8;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URISyntaxException;

public class SpaceImageFormatShould {

    @Test
    public void decode_sample_image() {
        var actual = new SpaceImageBuilder()
                .data("123456789012")
                .size(3, 2)
                .build();

        var expected = new SpaceImage(
                3, 2, new SpaceImageLayer("123", "456"),
                new SpaceImageLayer("789", "012")
        );
        Assertions.assertEquals(expected, actual);
        Assertions.assertEquals(actual.getLayers()[0], actual.getLayerWithFewest(0));
        Assertions.assertEquals(
                1,
                actual.getLayers()[0].count(1) * actual.getLayers()[0].count(2)
        );
    }

    @Test
    public void build_layer_full_of_2() {
        var layer = new SpaceImageBuilder()
                .fullOf(2)
                .size(3, 2)
                .build()
                .getLayers()[0];

        var expected = new SpaceImageLayer("222", "222");
        Assertions.assertEquals(expected, layer);
    }

    @Test
    public void render_sample_image() {
        var sample = new SpaceImageBuilder()
                .data("0222112222120000")
                .size(2, 2)
                .build();
        var expected = new SpaceImage(
                2, 2, new SpaceImageLayer("01", "10")
        );

        Assertions.assertEquals(expected, sample.render());
    }

    @Test
    public void decode_real_image() throws URISyntaxException, IOException {
        var image = new SpaceImageBuilder()
                .dataFromFile("day-8-input.txt")
                .size(25, 6)
                .build();
        Assertions.assertEquals(1340, image.getChecksum());
        System.out.println(image.render().toString()
                .replaceAll("1", "#")
                .replaceAll("0", " ")
        ); // LEJKC
    }
}
