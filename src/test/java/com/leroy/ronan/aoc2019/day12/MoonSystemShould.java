package com.leroy.ronan.aoc2019.day12;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

public class MoonSystemShould {

    @Test
    public void be_created_with_4_moons() {
        var system = new MoonSystem(List.of(
                "<x=-1, y=0, z=2>",
                "<x=2, y=-10, z=-7>",
                "<x=4, y=-8, z=8>",
                "<x=3, y=5, z=-1>"
        ));
        Assertions.assertEquals(4, system.getMoons().size());
        Assertions.assertEquals(-1, system.getMoons().get(0).getPosition().getX());
        Assertions.assertEquals(-10, system.getMoons().get(1).getPosition().getY());
        Assertions.assertEquals(8, system.getMoons().get(2).getPosition().getZ());
        Assertions.assertEquals(0, system.getMoons().get(0).getVelocity().getX());
        Assertions.assertEquals(0, system.getMoons().get(1).getVelocity().getY());
        Assertions.assertEquals(0, system.getMoons().get(2).getVelocity().getZ());

        Assertions.assertEquals((""+
                "pos=<x=-1, y=  0, z= 2>, vel=<x= 0, y= 0, z= 0>\n" +
                "pos=<x= 2, y=-10, z=-7>, vel=<x= 0, y= 0, z= 0>\n" +
                "pos=<x= 4, y= -8, z= 8>, vel=<x= 0, y= 0, z= 0>\n" +
                "pos=<x= 3, y=  5, z=-1>, vel=<x= 0, y= 0, z= 0>"
                ).replaceAll(" ", ""),
                system.toString()
            );
    }

    @Test
    public void simulate_a_step() {
        var system = new MoonSystem(List.of(
                "<x=-1, y=0, z=2>",
                "<x=2, y=-10, z=-7>",
                "<x=4, y=-8, z=8>",
                "<x=3, y=5, z=-1>"
        ));

        system.simulate();

        Assertions.assertEquals(("" +
                        "pos=<x= 2, y=-1, z= 1>, vel=<x= 3, y=-1, z=-1>\n" +
                        "pos=<x= 3, y=-7, z=-4>, vel=<x= 1, y= 3, z= 3>\n" +
                        "pos=<x= 1, y=-7, z= 5>, vel=<x=-3, y= 1, z=-3>\n" +
                        "pos=<x= 2, y= 2, z= 0>, vel=<x=-1, y=-3, z= 1>"
                ).replaceAll(" ", ""),
                system.toString()
        );
    }

    @Test
    public void simulate_10_step() {
        var system = new MoonSystem(List.of(
                "<x=-1, y=0, z=2>",
                "<x=2, y=-10, z=-7>",
                "<x=4, y=-8, z=8>",
                "<x=3, y=5, z=-1>"
        ));

        system.simulate(10);

        Assertions.assertEquals(("" +
                        "pos=<x= 2, y= 1, z=-3>, vel=<x=-3, y=-2, z= 1>\n" +
                        "pos=<x= 1, y=-8, z= 0>, vel=<x=-1, y= 1, z= 3>\n" +
                        "pos=<x= 3, y=-6, z= 1>, vel=<x= 3, y= 2, z=-3>\n" +
                        "pos=<x= 2, y= 0, z= 4>, vel=<x= 1, y=-1, z=-1>"
                ).replaceAll(" ", ""),
                system.toString()
        );

        Assertions.assertEquals(179, system.getEnergy());
    }

    @Test
    public void simulate_2772_step() {
        var system = new MoonSystem(List.of(
                "<x=-1, y=0, z=2>",
                "<x=2, y=-10, z=-7>",
                "<x=4, y=-8, z=8>",
                "<x=3, y=5, z=-1>"
        ));

        system.simulate(2772);
        var other = new MoonSystem(List.of(
                "<x=-1, y=0, z=2>",
                "<x=2, y=-10, z=-7>",
                "<x=4, y=-8, z=8>",
                "<x=3, y=5, z=-1>"
        ));

        Assertions.assertEquals(
                other.toString(),
                system.toString()
        );
    }

    @Test
    public void compute_stable_vector_as_9_14_22() {
        var system = new MoonSystem(List.of(
                "<x=-1, y=0, z=2>",
                "<x=2, y=-10, z=-7>",
                "<x=4, y=-8, z=8>",
                "<x=3, y=5, z=-1>"
        ));

        var original = new MoonSystem(List.of(
                "<x=-1, y=0, z=2>",
                "<x=2, y=-10, z=-7>",
                "<x=4, y=-8, z=8>",
                "<x=3, y=5, z=-1>"
        ));

        var stability = system.stabilize(original);

        Assertions.assertEquals(
                new Vector(36,28,44),
                stability
        );
        Assertions.assertEquals(
                2772,
                stability.getPPCM()
        );
    }

    @Test
    public void compute_9_steps_find_x_is_same() {
        var system = new MoonSystem(List.of(
                "<x=-1, y=0, z=2>",
                "<x=2, y=-10, z=-7>",
                "<x=4, y=-8, z=8>",
                "<x=3, y=5, z=-1>"
        ));

        system.simulate(18);

        var original = new MoonSystem(List.of(
                "<x=-1, y=0, z=2>",
                "<x=2, y=-10, z=-7>",
                "<x=4, y=-8, z=8>",
                "<x=3, y=5, z=-1>"
        ));
        Assertions.assertTrue(system.isComponentEquals(original, Vector::getX));
    }

    @Test
    public void stabilize_other_sample() throws IOException, URISyntaxException {
        var system = new MoonSystem(List.of(
                "<x=-8, y=-10, z=0>",
                "<x=5, y=5, z=10>",
                "<x=2, y=-7, z=3>",
                "<x=9, y=-8, z=-3>"
        ));
        var original = new MoonSystem(List.of(
                "<x=-8, y=-10, z=0>",
                "<x=5, y=5, z=10>",
                "<x=2, y=-7, z=3>",
                "<x=9, y=-8, z=-3>"
        ));
        var stability = system.stabilize(original);
        Assertions.assertEquals(
                new Vector(4056,5898,4702),
                stability
        );
        Assertions.assertEquals(
                9373549848l, // Wrong answer in the problem description ?
                stability.getPPCM()
        );
    }

    @Test
    public void simulate_real_input() throws IOException, URISyntaxException {
        var system = new MoonSystem("day-12-input.txt");
        system.simulate(1000);
        Assertions.assertEquals(10198, system.getEnergy());
    }



    @Test
    public void stabilize_real_input() throws IOException, URISyntaxException {
        var system = new MoonSystem("day-12-input.txt");
        var original = new MoonSystem("day-12-input.txt");
        var stability = system.stabilize(original);
        Assertions.assertEquals(
                new Vector(186028,161428,144624),
                stability
        );
        Assertions.assertEquals(
                271442326847376l,
                stability.getPPCM()
        );
    }
}
